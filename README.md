# SMMR: Sistema de monitoreo de material rodante

- [SMMR: Sistema de monitoreo de material rodante](#smmr-sistema-de-monitoreo-de-material-rodante)
  - [Hardware](#hardware)
    - [Minimal hardware for air quality sensors](#minimal-hardware-for-air-quality-sensors)
    - [Hardware for air quality sensors with GPS](#hardware-for-air-quality-sensors-with-gps)
  - [Installation](#installation)
    - [PlatformIo quick installation](#platformio-quick-installation)
    - [Configuration of scripts](#configuration-of-scripts)
    - [Compile and burn firmware](#compile-and-burn-firmware)
  - [Configuration](#configuration)
    - [Connect device to a network](#connect-device-to-a-network)
    - [Configure an Air sensor](#configure-an-air-sensor)
    - [Configure connection to a MQTT broker](#configure-connection-to-a-mqtt-broker)
  - [Local hacking](#local-hacking)
    - [Unit Testing](#unit-testing)
      - [Run all tests](#run-all-tests)
      - [Run a particular test](#run-a-particular-test)
    - [Check sanity of code](#check-sanity-of-code)
    - [Data format for Air Sensor](#data-format-for-air-sensor)
    - [Topics definitions](#topics-definitions)
    - [MQTT Commands](#mqtt-commands)
    - [Feedback](#feedback)

## Hardware

### Minimal hardware for air quality sensors

Recommended minimal hardware for Air Quality Sensing:

- ESP32 Dev Board
  - Espressif ESP32 DevkitC: <https://www.espressif.com/en/products/devkits/esp32-devkitc>
- Sensor board (one of them)
  - BME680 from Bosch
  - CCS811 from AMS

```mermaid
graph LR
  subgraph ESP32_Devkit[ESP32 DevKit]
    3.3V
    IO32
    IO33
    GND
  end
  3.3V --- Air_Sensor[Air Sensor]
  IO32 --scl--- Air_Sensor[Air Sensor]
  IO33 --sda--- Air_Sensor[Air Sensor]
  GND --- Air_Sensor[Air Sensor]
```

### Hardware for air quality sensors with GPS

You can add to the previosly descripted scheme a Serial GPS module as follows:

```mermaid
graph LR
  subgraph ESP32_Devkit[ESP32 DevKit]
    3.3V
    IO32
    IO33
    GND
    5V
    IO16
    IO17
  end
  3.3V --- Air_Sensor[Air Sensor]
  IO32 --scl--- Air_Sensor[Air Sensor]
  IO33 --sda--- Air_Sensor[Air Sensor]
  GND --- Air_Sensor[Air Sensor]

  5V --- GPS
  IO16 --RX--- GPS
  IO17 --TX--- GPS
  GND --- GPS
```

Note: Check if your GPS module needs 3.3V or 5V supply.

## Installation

### PlatformIo quick installation

You need pip package.

In debian/ubuntu run:

  ```bash
  apt-get install python-pip
  ```

In fedora/centos/redhat run:

  ```bash
  dnf install python-pip
  ```

Once pip is installed you need to install platformio running:

  ```bash
  pip install --upgrade --user platformio
  platformio platform install espressif32
  ```

### Configuration of scripts

1. Edit ./scripts/env file acording to ESP device (default /dev/ttyUSB0)

   ```bash
   PORT=/dev/ttyUSB0
   SPEED=115200
   ```

### Compile and burn firmware

1. Reset ESP from scratch

   ```bash
   ./scripts/erase.sh
   ```

1. Compile and Upload data FS and firmware

   ```bash
   ./scripts/again-full.sh
   ```

1. Connect to **smmr** Wifi network with **smmr0000** as password.
1. Open your browser with the following url <http://192.168.4.1> and connect to the user interface.

## Configuration

### Connect device to a network

1. Select tab configuración, sectión **Conectar a red WIFI**
1. Select option **habilitar**
1. Fill the SSID and **Contraseña** y click **Guardar**
1. Verify in the tab **Estado** the WiFi connection status.

### Configure an Air sensor

1. Select the tab  **Configuración**, section **Interfaces**
1. Enable the option **Habilitar sensor CO2** and click on **Guardar**
1. Select the tab **Estado** and check the sensor readings.

### Configure connection to a MQTT broker

1. Select the tab **Configuración**, section  **Servidor**
1. Select option mode to **MQTT** or the corresponding one.
1. Enter your server host, for ex **smmr.sguarin.com.ar**
1. Enter upper topic folder for your constellation of devices. For ex: **smmr**
1. Enter the assigner **user** and **password** for your device.
1. Click **Guardar**

## Local hacking

### Unit Testing

#### Run all tests

  ```bash
  platformio test -v
  ```

#### Run a particular test

  ```bash
  platformio test -f test_SystemConfig -v
  ```

### Check sanity of code

  ```bash
  platformio check
  ```

### Data format for Air Sensor

The data could be stored in microSD or sent via MQTT protocol in the following format:

```
Fecha,Latitud,Longitud,Altitud,CO2,TVOC,Temperatura,Humedad
```

### Topics definitions

- Se configura mediante la interfaz de usuario un 'base_topic' de la constelación.
- El SMMR envía los mensajes del sensado en el 'base_topic/[id sistema]'
- El SMMR se suscribe a 'base_topic/cmd/#' para recibir comandos globales para la constelación.
- El SMMR se suscribe a 'base_topic/id_sistema/cmd/#' para recibir comandos especificos para la unidad.
- El SMMR responde a 'base_topic/[id sistema]/config' respuestas de configuración
- El SMMR responde a 'base_topic/[id sistema]/status' respuestas de estado

### MQTT Commands

- '[...]/getconfig' y mensaje 1: el o los SMMR contesta con la configuración actual.
- '[...]/getstatus' y mensaje 1: el o los SMMR contesta con el estado actual.
- '[...]/setconfig' y mensaje parametro=valor&parametro2=valor... el o los SMMR se configuran con los parametros indicados.

### Feedback

For comments or improvements don't hesitate in contacting me.

If this was helpful in some kind I appreciate donations:

[![Donate](https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif)](https://www.paypal.com/donate?hosted_button_id=5FQP9N49Q3R4C)