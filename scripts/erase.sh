#!/bin/bash

BASE=`dirname "$0"`

echo "Importing $BASE/env"
. "$BASE/env"

~/.platformio/packages/tool-esptoolpy/esptool.py --chip esp32 --port "$PORT" --baud "$SPEED" erase_flash
