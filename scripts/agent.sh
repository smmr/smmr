#!/bin/bash

BASE=`dirname "$0"`

echo "Importing $BASE/env"
. "$BASE/env"

podman run -ti --rm \
	-e PLATFORMIO_AUTH_TOKEN \
	--device /dev/ttyUSB0:/dev/ttyUSB0:rwm \
	--group-add keep-groups \
	--security-opt label=disable \
	docker.io/shaguarger/platformio platformio remote agent start
