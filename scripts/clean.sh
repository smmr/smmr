#!/bin/bash

BASE=`dirname "$0"`

echo "Importing $BASE/env"
. "$BASE/env"

platformio run -t clean
