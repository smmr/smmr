#!/bin/bash

BASE=`dirname "$0"`

echo "Importing $BASE/env"
. "$BASE/env"

platformio run -t upload --upload-protocol espota --upload-port $@
