/******************************************************************************

  Sebastián Guarino
  https://gitlab.com/sguarin/smmr.git

  Distributed as-is; no warranty is given.
******************************************************************************/

#include "MyMQTT.h"
#include <FS.h>
#include <SPIFFS.h>

#define RETRY_INTERVAL 5000

#ifdef DEBUG
#ifndef DEBUG_ESP_PORT
#define DEBUG_ESP_PORT Serial
#endif
#ifndef DEBUG_ESP_WIFI
#define DEBUG_ESP_WIFI 1
#endif
#define DEBUG_MYMQTT(...)                   \
	DEBUG_ESP_PORT.print("DEBUG MyMQTT: "); \
	DEBUG_ESP_PORT.printf(__VA_ARGS__)
#else
#define DEBUG_MYMQTT(...)
#endif

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_MYMQTT(...)                  \
	INFO_ESP_PORT.print("INFO MyMQTT: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

MyMQTT mqtt;
THandlerGetConfigFunction pMyMQTTGetConfig = NULL;
THandlerSetConfigFunction pMyMQTTSetConfig = NULL;
THandlerGetStatusFunction pMyMQTTGetStatus = NULL;

/* Constructor */
MyMQTT::MyMQTT() : MQTTClient(MAX_PAYLOAD_SIZE)
{
	mqttMode = MyMQTTMode::MODE_MQTT;
	caPath = "/ca/ca.pem";
	listenCommands = false;
	pclient = this;
	lock = xSemaphoreCreateMutex();
	if (lock == 0)
	{
		// INFO_MYGPS("INFO MAIN: Error creating serial mutex\n");
	}
}

void MyMQTT::setCAPath(const char *newCAPath) {
	caPath = newCAPath;
}

/* init */
void MyMQTT::begin(const char *newHost, const char *newBrokerHost, const char *newUser, const char *newPassword,
				   const char *newBaseTopic, MyMQTTMode newMqttMode, bool newListenCommands)
{
	bool tls = false;

	host = String(newHost);
	brokerHost = String(newBrokerHost);
	user = String(newUser);
	password = String(newPassword);
	baseTopic = String(newBaseTopic);
	systemTopic = String(newBaseTopic) + "/" + String(newHost);
	listenCommands = newListenCommands;
	mqttMode = newMqttMode;

	if (connected())
	{
		disconnect();
	}

	/* FS start */
	if (!SPIFFS.begin())
	{
		INFO_MYMQTT("Error: could not mount SPIFFS\n");
	}

	if (mqttMode == MyMQTTMode::MODE_MQTTTLS)
	{
		tls = true;
		DEBUG_MYMQTT("Setting TLS without certificate validation\n");
		netssl.setInsecure();
	}

	if (mqttMode == MyMQTTMode::MODE_MQTTTLS_CA)
	{
		tls = true;
		DEBUG_MYMQTT("Setting TLS with certificate validation\n");
		
		File file = SPIFFS.open(caPath, "r");
		if (!netssl.loadCACert(file, file.size()))
		{
			DEBUG_MYMQTT("Error: loading CA certificate from SPIFFS\n");
		}
		file.close();
	}

	pclient->begin(brokerHost.c_str(), tls ? 8883 : 1883, tls ? netssl : net);

	/* configure hook */
	if (listenCommands)
		pclient->onMessage(commandReceived);
}

bool MyMQTT::connect(int32_t retries)
{
	bool ret = true;
	int32_t i = 0;
	do
	{
	} while (xSemaphoreTake(lock, portMAX_DELAY) != pdPASS);
	do
	{
		/* The setHandshakeTimeout is a quick workaround of a bug in espressif 4.2.0 */
		/* more info here: https://github.com/espressif/arduino-esp32/issues/6077 */
		if (mqttMode == MyMQTTMode::MODE_MQTTTLS || mqttMode == MyMQTTMode::MODE_MQTTTLS_CA)
			netssl.setHandshakeTimeout(30);
		ret = pclient->connect(host.c_str(), user.c_str(), password.c_str());
		if (!ret)
		{
			disconnect();
			delay(RETRY_INTERVAL);
			if (pclient->connected())
				break;
			DEBUG_MYMQTT("Retrying connect...\n");
		}
	} while (!ret && retries != -1 && i++ < retries);

	if (listenCommands)
	{
		/* subscribe for global commands */
		if (pclient->subscribe(baseTopic + "/cmd/#"))
		{
			DEBUG_MYMQTT("Subscribed for global commands successfull\n");
		}
		else
		{
			INFO_MYMQTT("Subscribed for global commands failed\n");
			ret = false;
		}

		/* subscribe for specific commands */
		if (pclient->subscribe(systemTopic + "/cmd/#"))
		{
			DEBUG_MYMQTT("Subscribed for specific commands successfull\n");
		}
		else
		{
			INFO_MYMQTT("Subscribed for specific commands failed\n");
			ret = false;
		}
	}
	xSemaphoreGive(lock);
	return ret;
}

bool MyMQTT::loop()
{
	bool ret;
	if (xSemaphoreTake(lock, 10 / portTICK_PERIOD_MS) != pdPASS)
		return false;
	ret = pclient->loop();
	xSemaphoreGive(lock);
	return ret;
}

void MyMQTT::commandReceived(String &topic, String &payload)
{
	String tmp;
	DEBUG_MYMQTT("Incoming message topic: %s, payload: %s\n", topic.c_str(), payload.c_str());
	if (topic.endsWith("/getstatus") && pMyMQTTGetStatus)
	{
		tmp.concat("status,");
		tmp.concat(pMyMQTTGetStatus());
		mqtt._publish(tmp, "/status");
	}

	if (topic.endsWith("/getconfig") && pMyMQTTGetConfig)
	{
		tmp.concat("config,");
		tmp = tmp.concat(pMyMQTTGetConfig());
		mqtt._publish(tmp, "/config");
	}

	if (topic.endsWith("/setconfig") && pMyMQTTSetConfig)
	{
		int attr, attrend, value, valueend, end;
		attr = 0;
		end = 0;

		do
		{
			attrend = payload.indexOf('=', attr);

#warning Ensure this + 1 will not overflow
			value = attrend + 1;
			valueend = payload.indexOf('&', value);

			/* till the end if , wasn't found */
			if (valueend == -1)
			{
				valueend = payload.length();
				end = 1;
			}

			pMyMQTTSetConfig(payload.substring(attr, attrend), payload.substring(value, valueend));
			attr = valueend + 1;
		} while (!end);

		DEBUG_MYMQTT("Setting CONFIG\n");
	}
}

void MyMQTT::setCommandsHooks(THandlerGetConfigFunction pGetConfigCallback, THandlerSetConfigFunction pSetConfigCallback, THandlerGetStatusFunction pGetStatusCallback)
{
	/* configure hooks */
	pMyMQTTGetConfig = pGetConfigCallback;
	pMyMQTTSetConfig = pSetConfigCallback;
	pMyMQTTGetStatus = pGetStatusCallback;
}

bool MyMQTT::publish(String &data)
{
	bool ret;
	lwmqtt_err_t err;
	if (!connected())
	{
		connect();
	}
	do
	{
	} while (xSemaphoreTake(lock, portMAX_DELAY) != pdPASS);
	ret = pclient->publish(systemTopic, data);
	err = lastError();
	xSemaphoreGive(lock);
	if (!ret)
	{
		INFO_MYMQTT("Error: in client.publish(): %d\n", err);
	}
	return ret;
}

bool MyMQTT::_publish(String &data, String suffixTopic)
{
	bool ret;
	lwmqtt_err_t err;
	if (!connected())
	{
		connect();
	}
	ret = pclient->publish((suffixTopic == "") ? systemTopic : systemTopic + suffixTopic, data);
	err = lastError();
	if (!ret)
	{
		INFO_MYMQTT("Error: in client.publish(): %d\n", err);
	}
	return ret;
}

bool MyMQTT::setCACert(const char *caCert)
{
	if (mqttMode == MyMQTTMode::MODE_MQTTTLS_CA)
	{
		netssl.setCACert(caCert);
		return true;
	}
	return false;
}

bool MyMQTT::end()
{
	bool ret = false;
	do
	{
	} while (xSemaphoreTake(lock, portMAX_DELAY) != pdPASS);
	if (connected())
	{
		ret = disconnect();
	}
	xSemaphoreGive(lock);
	return ret;
}
