/******************************************************************************

  Sebastián Guarino
  https://gitlab.com/sguarin/smmr.git

  Distributed as-is; no warranty is given.
******************************************************************************/

#ifndef _MYMQTT_H_
#define _MYMQTT_H_

#include <Arduino.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <MQTT.h>

#define MAX_PAYLOAD_SIZE 550

typedef std::function<String()> THandlerGetConfigFunction;
typedef std::function<bool(const String &config, const String &value)> THandlerSetConfigFunction;
typedef std::function<String()> THandlerGetStatusFunction;

enum class MyMQTTMode
{
  MODE_DISABLED = 1,
  MODE_MQTT = 2,
  MODE_MQTTTLS = 3,
  MODE_MQTTTLS_CA = 4
};

class MyMQTT : public MQTTClient
{
public:
  MyMQTT();
  void begin(const char *newhost, const char *newBrokerHost, const char *newUser, const char *newPassword,
             const char *newBaseTopic, MyMQTTMode newMqttMode = MyMQTTMode::MODE_MQTT, bool newListenCommands = false);
  bool connect(int32_t retries = -1);
  void setCommandsHooks(THandlerGetConfigFunction pGetConfigCallback, THandlerSetConfigFunction pSetConfigCallback, THandlerGetStatusFunction pGetStatusCallback);
  bool publish(String &data);
  bool setCACert(const char *caCert);
  void setCAPath(const char *newCAPath);
  bool loop();
  bool end();

protected:
  String host;
  String brokerHost;
  String user;
  String password;
  String baseTopic;
  String systemTopic;
  bool listenCommands;

private:
  SemaphoreHandle_t lock;
  const char *caPath;
  MyMQTTMode mqttMode;
  MQTTClient *pclient;
  WiFiClientSecure netssl;
  WiFiClient net;
  static void commandReceived(String &topic, String &payload);
  bool _publish(String &data, String suffixTopic = "");
};

extern MyMQTT mqtt;

#endif
