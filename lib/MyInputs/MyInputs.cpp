/******************************************************************************

  Sebastián Guarino
  https://gitlab.com/sguarin/smmr.git

  Distributed as-is; no warranty is given.
******************************************************************************/

#include "MyInputs.h"
#include <driver/adc.h>
#include <soc/sens_reg.h>
#include <soc/sens_struct.h>
/*==================[macros and definitions]=================================*/

#ifdef DEBUG
#ifndef DEBUG_ESP_PORT
#define DEBUG_ESP_PORT Serial
#endif
#ifndef DEBUG_ESP_WIFI
#define DEBUG_ESP_WIFI 1
#endif
#define DEBUG_MYINPUTS(...)                   \
	DEBUG_ESP_PORT.print("DEBUG MYINPUTS: "); \
	DEBUG_ESP_PORT.printf(__VA_ARGS__)
#else
#define DEBUG_MYINPUTS(...)
#endif

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_MYINPUTS(...)                  \
	INFO_ESP_PORT.print("INFO MYINPUTS: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

#define MYGPS_SERIAL Serial2

//#define MYINPUTS_A1_PORT GPIO_NUM_32
//#define MYINPUTS_A2_PORT GPIO_NUM_33
#define MYINPUTS_A1_PORT ADC1_CHANNEL_5
#define MYINPUTS_A2_PORT ADC1_CHANNEL_4
// #define MYINPUTS_D1_PORT GPIO_NUM_2
// #define MYINPUTS_D2_PORT GPIO_NUM_4
// #define MYINPUTS_D3_PORT GPIO_NUM_12
// #define MYINPUTS_D4_PORT GPIO_NUM_13

// Remapping pins for PCB routing optimization
//#define MYINPUTS_A1_PORT ADC2_CHANNEL_6
//#define MYINPUTS_A2_PORT ADC2_CHANNEL_5
#define MYINPUTS_D1_PORT GPIO_NUM_9
#define MYINPUTS_D2_PORT GPIO_NUM_13
#define MYINPUTS_D3_PORT GPIO_NUM_34
#define MYINPUTS_D4_PORT GPIO_NUM_36

#define DATA_QUEUE_SIZE 200

/*==================[internal data definition]===============================*/
portMUX_TYPE DRAM_ATTR timerMux = portMUX_INITIALIZER_UNLOCKED;
MyInputs inputs;
QueueHandle_t myInputsDataQueue = NULL;

/*==================[internal functions declaration]=========================*/
// https://www.toptal.com/embedded/esp32-audio-sampling
int IRAM_ATTR local_adc1_read(int channel) {
    uint16_t adc_value;
    SENS.sar_meas_start1.sar1_en_pad = (1 << channel); // only one channel is selected
    while (SENS.sar_slave_addr1.meas_status != 0);
    SENS.sar_meas_start1.meas1_start_sar = 0;
    SENS.sar_meas_start1.meas1_start_sar = 1;
    while (SENS.sar_meas_start1.meas1_done_sar == 0);
    adc_value = SENS.sar_meas_start1.meas1_data_sar;
    return adc_value;
}

void IRAM_ATTR onTimer()
{
	BaseType_t rv;
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	myInputsDataItem_t myInputsDataItem;
	portENTER_CRITICAL_ISR(&timerMux);

	myInputsDataItem.a[0] = local_adc1_read(MYINPUTS_A1_PORT);
	//myInputsDataItem.a[1] = local_adc1_read(MYINPUTS_A2_PORT);
	myInputsDataItem.d[0] = digitalRead(MYINPUTS_D1_PORT);
	myInputsDataItem.d[1] = digitalRead(MYINPUTS_D2_PORT);
	myInputsDataItem.d[2] = digitalRead(MYINPUTS_D3_PORT);
	myInputsDataItem.d[3] = digitalRead(MYINPUTS_D4_PORT);

	rv = xQueueSendFromISR(myInputsDataQueue, &myInputsDataItem, &xHigherPriorityTaskWoken);

	if (xHigherPriorityTaskWoken == pdTRUE)
	{
		portYIELD_FROM_ISR();
	}
	portEXIT_CRITICAL_ISR(&timerMux);
}

/*==================[internal functions declaration]=========================*/

/* Constructor */
MyInputs::MyInputs()
{
	timer = NULL;
	myInputsDataQueue = NULL;
}

bool MyInputs::begin(uint8_t newRate)
{
	bool ret = true;
    int read_raw;
	
	rate = newRate;

	// create queue for data to transmit via mqtt
	if (myInputsDataQueue == NULL)
		myInputsDataQueue = xQueueCreate(DATA_QUEUE_SIZE, sizeof(myInputsDataItem_t));
	if (myInputsDataQueue == 0)
	{
		INFO_MYINPUTS("Error creating transmit queue\n");
		ret = false;
	}

    //adc1_config_width(ADC_WIDTH_BIT_12);
    //adc1_config_channel_atten(MYINPUTS_A1_PORT, ADC_ATTEN_DB_0);
	adc1_get_raw(MYINPUTS_A1_PORT);
	//adc1_get_raw(MYINPUTS_A2_PORT);

	pinMode(MYINPUTS_D1_PORT, INPUT);
	pinMode(MYINPUTS_D2_PORT, INPUT);
	pinMode(MYINPUTS_D3_PORT, INPUT);
	pinMode(MYINPUTS_D4_PORT, INPUT);

	// Use 1st timer of 4 (counted from zero).
	// Set 80 divider for prescaler (see ESP32 Technical Reference Manual for more
	// info).
	timer = timerBegin(0, 80, true);

	// Attach onTimer function to our timer.
	timerAttachInterrupt(timer, &onTimer, true);

	// Set alarm to call onTimer function every x (value in microseconds).
	timerAlarmWrite(timer, 1000000/rate, true);

	// Start the alarm
	timerAlarmEnable(timer);

	return ret;
}

bool MyInputs::read(myInputsDataItem_t *pmyInputsDataItem)
{
	BaseType_t rv;
	if (myInputsDataQueue == NULL)
		return false;
	rv = xQueueReceive(myInputsDataQueue, pmyInputsDataItem, portMAX_DELAY);
	if (rv != pdPASS)
	{
		INFO_MYINPUTS("Error: getting data from dataStoreQueue\n");
		return false;
	}
	return true;
}

void MyInputs::end()
{
	if (timer != NULL) {
		timerAlarmDisable(timer);
		timer = NULL;
	}
	if (myInputsDataQueue != NULL) {
		vQueueDelete(myInputsDataQueue);
		myInputsDataQueue = NULL;
	}
}
