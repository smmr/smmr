/******************************************************************************

  Sebastián Guarino
  https://gitlab.com/sguarin/smmr.git

  Distributed as-is; no warranty is given.
******************************************************************************/

#ifndef _MYINPUTS_H_
#define _MYINPUTS_H_

#include <Arduino.h>

typedef struct myInputsDataItemStruct
{
	uint16_t a[2];
  uint8_t d[4];
} myInputsDataItem_t;

class MyInputs
{
  public:
    MyInputs();
    bool begin(uint8_t rate);
    bool read(myInputsDataItem_t *pmyInputsDataItem);
    void end();
    
  private:
    hw_timer_t * timer;
    portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;
    uint8_t rate;
};

extern MyInputs inputs;

#endif
