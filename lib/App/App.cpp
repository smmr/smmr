/******************************************************************************

  Sebastián Guarino
  https://gitlab.com/sguarin/smmr.git

  Distributed as-is; no warranty is given.
******************************************************************************/

#include "App.h"
#include <MySD.h>
#include <MySensors.h>
#include <MyInputs.h>

/*==================[macros and definitions]=================================*/

#define DATA_STORE_QUEUE_SIZE 5
#define DATA_TRANSMIT_QUEUE_SIZE 10

#define SERIAL_MUTEX_LOCK() \
	do                      \
	{                       \
	} while (xSemaphoreTake(serialLock, portMAX_DELAY) != pdPASS)
#define SERIAL_MUTEX_UNLOCK() xSemaphoreGive(serialLock)

#if DEBUG
#ifndef DEBUG_ESP_PORT
#define DEBUG_ESP_PORT Serial
#endif
#ifndef DEBUG_ESP_WIFI
#define DEBUG_ESP_WIFI 1
#endif
#define DEBUG_APP(...)                   \
	DEBUG_ESP_PORT.print("DEBUG App: "); \
	DEBUG_ESP_PORT.printf(__VA_ARGS__)
#else
#define DEBUG_APP(...)
#endif

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_APP(...)                  \
	INFO_ESP_PORT.print("INFO App: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

/*==================[internal data definition]===============================*/
App app;

/*==================[internal functions declaration]=========================*/

/*==================[internal functions declaration]=========================*/

//! Task function to read data from sensors and push to Queues
/*!
 * \param pvParameters FreeRTOS task arguments
 */
void App::inputsTask(void *pvParameters)
{
	BaseType_t rv;
	App *p = static_cast<App *>(pvParameters);
	myInputsDataItem_t myInputsDataItem;
	dataItem_t dataItem[6];
	uint32_t u = 0;
	bool ret;

	strncpy(dataItem[0].id, "a1", sizeof(dataItem[0].id));
	strncpy(dataItem[1].id, "a2", sizeof(dataItem[1].id));
	strncpy(dataItem[2].id, "d1", sizeof(dataItem[2].id));
	strncpy(dataItem[3].id, "d2", sizeof(dataItem[3].id));
	strncpy(dataItem[4].id, "d3", sizeof(dataItem[4].id));
	strncpy(dataItem[5].id, "d4", sizeof(dataItem[5].id));

	while (1)
	{
		ret = false;
		for (u = 0; u < 6; u++)
		{
			p->fillGpsData(&dataItem[u]);
			dataItem[u].rate = p->inputs_rate;
		}

		for (u = 0; u < p->inputs_rate; u++)
		{
			ret = inputs.read(&myInputsDataItem);
			if (!ret)
			{
				INFO_APP("Error: getting data from dataStoreQueue\n");
			}
			else
			{
				dataItem[0].input[u] = myInputsDataItem.a[0];
				dataItem[1].input[u] = myInputsDataItem.a[1];
				dataItem[2].input[u] = myInputsDataItem.d[0];
				dataItem[3].input[u] = myInputsDataItem.d[1];
				dataItem[4].input[u] = myInputsDataItem.d[2];
				dataItem[5].input[u] = myInputsDataItem.d[3];
			}
		}
		if (ret)
		{
			p->a1_value = myInputsDataItem.a[0];
			p->a2_value = myInputsDataItem.a[1];
			p->d1_value = myInputsDataItem.d[0];
			p->d2_value = myInputsDataItem.d[1];
			p->d3_value = myInputsDataItem.d[2];
			p->d4_value = myInputsDataItem.d[3];
		}

		if (p->mem_enable)
		{
			rv = pdTRUE;
			if (p->a1_enable)
			{
				rv &= xQueueSend(p->dataStoreQueue, &dataItem[0], portMAX_DELAY);
			}
			if (p->a2_enable)
			{
				rv &= xQueueSend(p->dataStoreQueue, &dataItem[1], portMAX_DELAY);
			}
			if (p->d1_enable)
			{
				rv &= xQueueSend(p->dataStoreQueue, &dataItem[2], portMAX_DELAY);
			}
			if (p->d2_enable)
			{
				rv &= xQueueSend(p->dataStoreQueue, &dataItem[3], portMAX_DELAY);
			}
			if (p->d3_enable)
			{
				rv &= xQueueSend(p->dataStoreQueue, &dataItem[4], portMAX_DELAY);
			}
			if (p->d4_enable)
			{
				rv &= xQueueSend(p->dataStoreQueue, &dataItem[5], portMAX_DELAY);
			}
			if (rv != pdTRUE)
			{
				INFO_APP("Error: pushing to dataStoreQueue\n");
			}
		}
		if (p->server_mode != MyMQTTMode::MODE_DISABLED)
		{
			rv = pdTRUE;
			if (p->a1_enable)
			{
				rv &= xQueueSend(p->dataTransmitQueue, &dataItem[0], portMAX_DELAY);
			}
			if (p->a2_enable)
			{
				rv &= xQueueSend(p->dataTransmitQueue, &dataItem[1], portMAX_DELAY);
			}
			if (p->d1_enable)
			{
				rv &= xQueueSend(p->dataTransmitQueue, &dataItem[2], portMAX_DELAY);
			}
			if (p->d2_enable)
			{
				rv &= xQueueSend(p->dataTransmitQueue, &dataItem[3], portMAX_DELAY);
			}
			if (p->d3_enable)
			{
				rv &= xQueueSend(p->dataTransmitQueue, &dataItem[4], portMAX_DELAY);
			}
			if (p->d4_enable)
			{
				rv = xQueueSend(p->dataTransmitQueue, &dataItem[5], portMAX_DELAY);
			}
			if (rv != pdTRUE)
			{
				INFO_APP("Error: pushing to dataTransmitQueue\n");
			}
		}
	}
}

void App::sensorsTask(void *pvParameters)
{
	BaseType_t rv;
	App *p = static_cast<App *>(pvParameters);
	dataItem_t dataItem;
	bool dataReady = false;

	while (true)
	{
		if (p->sensor_co2 && sensors.update())
		{
			strncpy(dataItem.id, "co2", sizeof(dataItem.id));

			// fill sensor co2 data
			dataItem.co2 = sensors.getCO2();
			p->co2_value = dataItem.co2;
			dataItem.tvoc = sensors.getTVOC();
			p->tvoc_value = dataItem.tvoc;
			dataItem.temp = sensors.getTemp();
			p->temp_value = dataItem.temp;
			dataItem.hum = sensors.getHum();
			p->hum_value = dataItem.hum;
			dataItem.pres = sensors.getPres();
			p->pres_value = dataItem.pres;
			dataReady = true;
		}

		if (dataReady)
		{
			p->fillGpsData(&dataItem);

			if (p->mem_enable)
			{
				DEBUG_APP("Pushing data to dataStoreQueue\n");
				rv = xQueueSend(p->dataStoreQueue, &dataItem, portMAX_DELAY);
				if (rv != pdPASS)
				{
					INFO_APP("Error: pushing to dataStoreQueue\n");
				}
			}

			if (p->server_mode != MyMQTTMode::MODE_DISABLED)
			{
				DEBUG_APP("Pushing data to dataTransmitQueue\n");
				rv = xQueueSend(p->dataTransmitQueue, &dataItem, portMAX_DELAY);
				if (rv != pdPASS)
				{
					INFO_APP("Error: pushing to dataTransmitQueue\n");
				}
			}
			dataReady = false;
		}
	}
}

//! Task to dequeue data from dataStoreQueue and write to SD
/*!
	\param pvParameters its FreeRTOS arg
 */
void App::SDTask(void *pvParameters)
{
	BaseType_t rv;
	App *p = static_cast<App *>(pvParameters);
	dataItem_t dataItem;
	String dataLine;
	while (1)
	{
		rv = xQueueReceive(p->dataStoreQueue, &dataItem, portMAX_DELAY);
		if (rv != pdPASS)
		{
			INFO_APP("Error: getting data from dataStoreQueue\n");
		}
		else
		{
			dataLine = p->data2str(&dataItem);
			dataLine.concat('\n');
			sd.appendLine(dataLine.c_str());
			DEBUG_APP("Saving sample:%s", dataLine.c_str());
		}
	}
}

//! Task to dequeue data from dataTransmitQueue and transmit
/*!
	\param pvParameters its FreeRTOS arg
 */
void App::transmitTask(void *pvParameters)
{
	BaseType_t rv;
	App *p = static_cast<App *>(pvParameters);
	dataItem_t dataItem;
	String dataLine;
	while (1)
	{
		rv = xQueueReceive(p->dataTransmitQueue, &dataItem, portMAX_DELAY);
		if (rv != pdPASS)
		{
			INFO_APP("Error: getting data from dataTransmitQueue\n");
		}
		else
		{
			dataLine = p->data2str(&dataItem);
			p->mqttPublish(dataLine);
		}
	}
}

String App::data2str(dataItem_t *dataItem)
{
	String dataLine;
	char geoPoint[30];
	uint8_t u;
	dataLine = host;
	dataLine.concat(",");

	dataLine.concat((const char *)dataItem->time);
	dataLine.concat(",");
	if (dataItem->lat == 0 && dataItem->lng == 0 && dataItem->alt == 0)
	{
		dataLine.concat(",,");
	}
	else
	{
		// TODO BUG Report upstream Arduino String concat double
		snprintf(geoPoint, 29, "%f,%f", dataItem->lat, dataItem->lng);
		dataLine.concat(geoPoint);
		//dataLine.concat(dataItem->lat);
		//dataLine.concat(",");
		//dataLine.concat(dataItem->lng);
		dataLine.concat(",");
		dataLine.concat(dataItem->alt);
	}
	dataLine.concat(",");
	dataLine.concat(dataItem->id);
	dataLine.concat(",");
	if (!strcmp(dataItem->id, "co2"))
	{
		dataLine.concat(dataItem->co2);
		dataLine.concat(",");
		dataLine.concat(dataItem->tvoc);
		dataLine.concat(",");
		dataLine.concat(dataItem->temp);
		dataLine.concat(",");
		dataLine.concat(dataItem->hum);
		dataLine.concat(",");
		dataLine.concat(dataItem->pres);
	}
	else if (!strncmp(dataItem->id, "a", 1) || !strncmp(dataItem->id, "d", 1))
	{
		dataLine.concat(dataItem->rate);
		for (u = 0; u < dataItem->rate; u++)
		{
			dataLine.concat(",");
			dataLine.concat(dataItem->input[u]);
		}
	}

	return dataLine;
}

void App::fillGpsData(dataItem_t *pdataItem)
{
	if (gps_enable && gps.getSatellites() >= 1)
	{
		// fill gps data
		strncpy(pdataItem->time, gps.getTimeStr().c_str(), 20);
		pdataItem->lat = gps.getLat();
		pdataItem->lng = gps.getLng();
		pdataItem->alt = gps.getAlt();
	}
	else
	{
		struct tm timeinfo;
		time_t now;
		pdataItem->time[0] = '\0';
		time(&now);
		localtime_r(&now, &timeinfo);
		snprintf(pdataItem->time, 20, "%04u-%02u-%02u %02u:%02u:%02u", timeinfo.tm_year != 70 ? timeinfo.tm_year + 1900 : 2020, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);

		pdataItem->lat = 0;
		pdataItem->lng = 0;
		pdataItem->alt = 0;
	}
}

/* Constructor */
App::App()
{
	sensorsTaskHandle = NULL;
	SDTaskHandle = NULL;
	transmitTaskHandle = NULL;
	inputsTaskHandle = NULL;

	dataStoreQueue = NULL;
	dataTransmitQueue = NULL;

	mem_enable = false;
	gps_enable = false;
	sensor_co2 = false;

	a1_enable = false;
	a2_enable = false;
	d1_enable = false;
	d2_enable = false;
	d3_enable = false;
	d4_enable = false;
	inputs_rate = 0;

	a1_value = 0;
	a2_value = 0;
	d1_value = 0;
	d2_value = 0;
	d3_value = 0;
	d4_value = 0;

	host = "";
	server_mode = MyMQTTMode::MODE_DISABLED;
	server_host = "";
	server_topic = "";
	server_user = "";
	server_password = "";
	server_commands = false;
}

bool App::init()
{
	bool ret = true;

	// GPS initialization
	if (gps_enable)
	{
		DEBUG_APP("Initializing GPS\n");
		gps.begin();
	}

	// create queue for data to store in sd
	if (dataStoreQueue == NULL)
		dataStoreQueue = xQueueCreate(DATA_STORE_QUEUE_SIZE, sizeof(dataItem_t));
	if (dataStoreQueue == NULL)
	{
		INFO_APP("Error creating store queue\n");
		ret = false;
	}

	// create queue for data to transmit via mqtt
	if (dataTransmitQueue == NULL)
		dataTransmitQueue = xQueueCreate(DATA_TRANSMIT_QUEUE_SIZE, sizeof(dataItem_t));
	if (dataTransmitQueue == 0)
	{
		INFO_APP("Error creating transmit queue\n");
		ret = false;
	}

	// I2C and sensors initialization
	if (sensor_co2)
	{
		DEBUG_APP("Initializing Sensors\n");
		if (!sensors.init())
		{
			INFO_APP("Error initializing Sensors\n");
		}
	}

	// sensor task
	if (sensorsTaskHandle == NULL && sensor_co2)
	{
		xTaskCreatePinnedToCore(
			sensorsTask,		/* Function to implement the task */
			"sensorsTask",		/* Name of the task */
			10000,				/* Stack size in words */
			this,				/* Task input parameter */
			1,					/* Priority of the task */
			&sensorsTaskHandle, /* Task handle. */
			1);					/* Core where the task should run */
	}
	if (sensorsTaskHandle != NULL && !sensor_co2)
	{
		vTaskDelete(sensorsTaskHandle);
		sensorsTaskHandle = NULL;
	}

	// SPI and SD initialization
	if (mem_enable)
	{
		DEBUG_APP("Initializing SD\n");
		sd.init();
	}
	// SD task
	if (SDTaskHandle == NULL && mem_enable)
	{
		xTaskCreatePinnedToCore(
			SDTask,		   /* Function to implement the task */
			"SDTask",	   /* Name of the task */
			10000,		   /* Stack size in words */
			this,		   /* Task input parameter */
			2,			   /* Priority of the task */
			&SDTaskHandle, /* Task handle. */
			1);			   /* Core where the task should run */
	}
	if (SDTaskHandle != NULL && !mem_enable)
	{
		vTaskDelete(SDTaskHandle);
		SDTaskHandle = NULL;
	}

	// MQTT Client initialization
	if (server_mode != MyMQTTMode::MODE_DISABLED)
	{
		DEBUG_APP("Initializing MQTT\n");
		mqtt.begin(host.c_str(), server_host.c_str(), server_user.c_str(), server_password.c_str(), server_topic.c_str(), server_mode, server_commands);
		mqtt.connect();
	}

	// MQTT Task create
	if (transmitTaskHandle == NULL && server_mode != MyMQTTMode::MODE_DISABLED)
	{
		xTaskCreatePinnedToCore(
			transmitTask,		 /* Function to implement the task */
			"transmitTask",		 /* Name of the task */
			10000,				 /* Stack size in words */
			this,				 /* Task input parameter */
			2,					 /* Priority of the task */
			&transmitTaskHandle, /* Task handle. */
			1);					 /* Core where the task should run */
	}
	// MQTT task delete
	if (transmitTaskHandle != NULL && server_mode == MyMQTTMode::MODE_DISABLED)
	{
		vTaskDelete(transmitTaskHandle);
		transmitTaskHandle = NULL;
	}

	// MyInputsTask
	if (a1_enable || a2_enable || d1_enable || d2_enable || d3_enable || d4_enable)
	{
		INFO_APP("Initializing inputs");
		ret &= inputs.begin(inputs_rate);
		if (inputsTaskHandle == NULL)
		{
			xTaskCreatePinnedToCore(
				inputsTask,		   /* Function to implement the task */
				"inputsTask",	   /* Name of the task */
				10000,			   /* Stack size in words */
				this,			   /* Task input parameter */
				2,				   /* Priority of the task */
				&inputsTaskHandle, /* Task handle. */
				1);				   /* Core where the task should run */
		}
	}

	INFO_APP("Initialization complete\n");
	return ret;
}

void App::end()
{
	// GPS
	gps.end();

	// Inputs
	if (inputsTaskHandle != NULL)
		vTaskDelete(inputsTaskHandle);
	inputsTaskHandle = NULL;
	inputs.end();

	// Sensors
	if (sensorsTaskHandle != NULL)
		vTaskDelete(sensorsTaskHandle);
	sensorsTaskHandle = NULL;
}

void App::setMemEnable(bool new_mem_enable)
{
	mem_enable = new_mem_enable;
}

void App::setGpsEnable(bool new_gps_enable)
{
	gps_enable = new_gps_enable;
}

void App::setSensorCo2(bool new_sensor_co2)
{
	sensor_co2 = new_sensor_co2;
}

void App::setHost(const String &new_host)
{
	host = new_host;
}

void App::setA1Enable(bool new_a1_enable)
{
	a1_enable = new_a1_enable;
}

void App::setA2Enable(bool new_a2_enable)
{
	a2_enable = new_a2_enable;
}

void App::setD1Enable(bool new_d1_enable)
{
	d1_enable = new_d1_enable;
}

void App::setD2Enable(bool new_d2_enable)
{
	d2_enable = new_d2_enable;
}

void App::setD3Enable(bool new_d3_enable)
{
	d3_enable = new_d3_enable;
}

void App::setD4Enable(bool new_d4_enable)
{
	d4_enable = new_d4_enable;
}

void App::setInputsRate(uint8_t new_inputs_rate)
{
	inputs_rate = new_inputs_rate;
}

void App::setServerMode(MyMQTTMode new_server_mode)
{
	server_mode = new_server_mode;
}

void App::setServerHost(const String &new_server_host)
{
	server_host = new_server_host;
}

void App::setServerTopic(const String &new_server_topic)
{
	server_topic = new_server_topic;
}

void App::setServerUser(const String &new_server_user)
{
	server_user = new_server_user;
}

void App::setServerPassword(const String &new_server_password)
{
	server_password = new_server_password;
}

void App::setServerCommands(bool new_server_commands)
{
	server_commands = new_server_commands;
}

bool App::mqttPublish(String &message)
{
	return mqtt.publish(message);
}

String App::getStatus()
{
	String status = "system_status=";
#warning TODO
	status.concat("OK");

	status.concat("&");
	status.concat(FPSTR(GC_SYSTEM_MODEL));
	status.concat("=");
	status.concat(FPSTR(GC_SYSTEM_MODEL_VALUE));

	status.concat("&");
	status.concat(FPSTR(GC_SYSTEM_VERSION));
	status.concat("=");
	status.concat(FPSTR(GC_SYSTEM_VERSION_VALUE));

	status.concat("&");
	status.concat(FPSTR(GC_SYSTEM_SERIAL));
	status.concat("=");
	status.concat(FPSTR(GC_SYSTEM_SERIAL_VALUE));

	status.concat("&");
	status.concat(FPSTR(GC_SYSTEM_NAME));
	status.concat("=");
	status.concat(host);

	if (mem_enable)
	{
		status.concat("&");
		status.concat(FPSTR(GC_SD_STATUS));
		status.concat("=");
		status.concat(sd.status);
		status.concat("&");
		status.concat(FPSTR(GC_SD_SIZE));
		status.concat("=");
		status.concat(sd.getTotalBytes());
		status.concat("&");
		status.concat(FPSTR(GC_SD_FREE));
		status.concat("=");
		status.concat(sd.getFreeBytes());
	}

	if (gps_enable)
	{
		status.concat("&");
		status.concat(FPSTR(GC_GPS_STATUS));
		status.concat("=");
		status.concat(gps.getStatusStr());

		status.concat("&");
		status.concat(FPSTR(GC_GPS_SAT_COUNT));
		status.concat("=");
		status.concat(gps.getSatellites());
	}
	if (sensor_co2)
	{
		status.concat("&");
		status.concat(FPSTR("co2_value"));
		status.concat("=");
		status.concat(co2_value);
		status.concat("&");
		status.concat(FPSTR("tvoc_value"));
		status.concat("=");
		status.concat(tvoc_value);
		status.concat("&");
		status.concat(FPSTR("temp_value"));
		status.concat("=");
		status.concat(temp_value);
		status.concat("&");
		status.concat(FPSTR("pres_value"));
		status.concat("=");
		status.concat(pres_value);
		status.concat("&");
		status.concat(FPSTR("hum_value"));
		status.concat("=");
		status.concat(hum_value);
	}
	if (a1_enable)
	{
		status.concat("&");
		status.concat(FPSTR("a1_value"));
		status.concat("=");
		status.concat(a1_value);
	}
	if (a2_enable)
	{
		status.concat("&");
		status.concat(FPSTR("a2_value"));
		status.concat("=");
		status.concat(a2_value);
	}
	if (d1_enable)
	{
		status.concat("&");
		status.concat(FPSTR("d1_value"));
		status.concat("=");
		status.concat(d1_value);
	}
	if (d2_enable)
	{
		status.concat("&");
		status.concat(FPSTR("d2_value"));
		status.concat("=");
		status.concat(d2_value);
	}
	if (d3_enable)
	{
		status.concat("&");
		status.concat(FPSTR("d3_value"));
		status.concat("=");
		status.concat(d3_value);
	}
	if (d4_enable)
	{
		status.concat("&");
		status.concat(FPSTR("d4_value"));
		status.concat("=");
		status.concat(d4_value);
	}
	return status;
}