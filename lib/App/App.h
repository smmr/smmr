
#ifndef _APP_H_
#define _APP_H_

#include <Arduino.h>
#include <HardwareSerial.h>
#include <MyGPS.h>
#include <MyMQTT.h>

#ifndef GC_APP_ID
#define GC_APP_ID "smmr"
#endif

#ifndef GC_SYSTEM_MODEL_VALUE
#define GC_SYSTEM_MODEL_VALUE "SMMR"
#endif
#ifndef GC_SYSTEM_VERSION_VALUE
#define GC_SYSTEM_VERSION_VALUE "1.00"
#endif
#ifndef GC_SYSTEM_SERIAL_VALUE
#define GC_SYSTEM_SERIAL_VALUE "000001"
#endif

const char GC_SYSTEM_MODEL[] PROGMEM = "system_model";
const char GC_SYSTEM_VERSION[] PROGMEM = "system_version";
const char GC_SYSTEM_SERIAL[] PROGMEM = "system_serial";
const char GC_SYSTEM_NAME[] PROGMEM = "system_name";

const char GC_GPS_STATUS[] PROGMEM = "gps_status";
const char GC_GPS_SAT_COUNT[] PROGMEM = "gps_sat_count";

const char GC_SD_STATUS[] PROGMEM = "sd_status";
const char GC_SD_SIZE[] PROGMEM = "sd_size";
const char GC_SD_FREE[] PROGMEM = "sd_free";

typedef struct dataItemStruct
{
	char time[20]; // DD/MM/AAAA HH:MM:SS

  char id[4]; // ID=co2, a1, a2, d1, d2, d3, d4

	double lat;
	double lng;
	double alt;

  uint8_t rate;
  uint16_t input[100];
  
	uint16_t co2;  // PPM
	uint16_t tvoc; // PPM
	float temp;
	float pres;
	float hum;
} dataItem_t;

class App
{
public:
  App();

  bool init();
  void end();

  void setMemEnable(bool new_mem_enable);
  void setGpsEnable(bool new_gps_enable);
  void setSensorCo2(bool new_sensor_co2);

  void setA1Enable(bool new_a1_enable);
  void setA2Enable(bool new_a2_enable);
  void setD1Enable(bool new_d1_enable);
  void setD2Enable(bool new_d2_enable);
  void setD3Enable(bool new_d3_enable);
  void setD4Enable(bool new_d4_enable);
  void setInputsRate(uint8_t new_inputs_rate);

  void setHost(const String &new_host);
  void setServerMode(MyMQTTMode new_server_mode);
  void setServerHost(const String &new_server_host);
  void setServerTopic(const String &new_server_topic);
  void setServerUser(const String &new_server_user);
  void setServerPassword(const String &new_server_password);
  void setServerCommands(bool new_server_commands);

  String getStatus();

  bool mqttPublish(String &message);

protected:
  static void transmitTask(void *pvParameters);
  static void sensorsTask(void *pvParameters);
  static void SDTask(void *pvParameters);
  static void inputsTask(void *pvParameters);

private:
  TaskHandle_t sensorsTaskHandle;
  TaskHandle_t SDTaskHandle;
  TaskHandle_t transmitTaskHandle;
  TaskHandle_t inputsTaskHandle;

  QueueHandle_t dataStoreQueue;
  QueueHandle_t dataTransmitQueue;

  bool mem_enable;
  bool gps_enable;
  bool sensor_co2;

  bool a1_enable;
  bool a2_enable;
  bool d1_enable;
  bool d2_enable;
  bool d3_enable;
  bool d4_enable;
  uint8_t inputs_rate;

  uint16_t co2_value;
	uint16_t tvoc_value;
	float temp_value;
	float pres_value;
	float hum_value;
  uint16_t a1_value;
  uint16_t a2_value;
  uint8_t d1_value;
  uint8_t d2_value;
  uint8_t d3_value;
  uint8_t d4_value;

  MyMQTTMode server_mode;
  String host;
  String server_host;
  String server_topic;
  String server_user;
  String server_password;
  bool server_commands;

  String data2str(dataItem_t *pdataItem);
  void fillGpsData(dataItem_t *pdataItem);
};

extern App app;

#endif
