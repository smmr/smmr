
#ifndef __MySensors_h__
#define __MySensors_h__

#include <Arduino.h>

#define MYSENSORS_MODE_AUTO   0
#define MYSENSORS_MODE_CCS811 1
#define MYSENSORS_MODE_BME680 2
#define MYSENSORS_MODE_MHZ19 3

class MySensors
{
public:
	MySensors();

	bool init(uint8_t mode = MYSENSORS_MODE_AUTO);
	bool update();

	uint16_t getCO2();
	uint16_t getTVOC();
	float getTemp();
	float getHum();
	float getPres();
	float getIaq();
	uint8_t getIaqAccuracy();

protected:
	// Variables
	float temperature;
	float pressure;
	float humidity;
	float iaq;
	uint8_t iaqAccuracy;
	uint16_t co2;
	uint16_t tvoc;

private:
	bool initialized;
	uint8_t currentMode;

	bool initCCS811();
	bool initBME680();
	bool checkBME680();
	bool initMHZ19();
};

extern MySensors sensors;

#endif
