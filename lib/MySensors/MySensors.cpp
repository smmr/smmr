/******************************************************************************

  Sebastián Guarino
  https://gitlab.com/sguarin/smmr.git

  Distributed as-is; no warranty is given.
******************************************************************************/

#include "MySensors.h"
#include <Wire.h>
#include <ccs811.h>
#include <Adafruit_BMP280.h>
#include <ClosedCube_HDC1080.h>
#include <bsec.h>
#include <SoftwareSerial.h>
#include <MHZ19.h>

/*==================[macros and definitions]=================================*/

// #define CCS811_ADDR 0x5B //Default I2C Address
#define CCS811_ADDR 0x5A // Alternate I2C Address
#define HDC1080_ADDR 0x40

// #define BME680_ADDR 0x76
#define BME680_ADDR 0x77

/* SMMR sd 1 wire board */
// #define I2C_SDA GPIO_NUM_21
// #define I2C_SCL GPIO_NUM_22

/* SMMR sd 4 wire board Rev 2.0 */
// #define I2C_SDA GPIO_NUM_21
// #define I2C_SCL GPIO_NUM_19

/* SMMR sd 4 wire board Rev 2.1 */
/* I2C definitions */
#define I2C_SDA GPIO_NUM_33
#define I2C_SCL GPIO_NUM_32

/* Soft serial definitions */
#define SER_TX GPIO_NUM_33
#define SER_RX GPIO_NUM_32

#ifdef DEBUG
#ifndef DEBUG_ESP_PORT
#define DEBUG_ESP_PORT Serial
#endif
#ifndef DEBUG_ESP_WIFI
#define DEBUG_ESP_WIFI 1
#endif
#define DEBUG_MYSENSORS(...)                   \
	DEBUG_ESP_PORT.print("DEBUG mySensors: "); \
	DEBUG_ESP_PORT.printf(__VA_ARGS__)
#else
#define DEBUG_MYSENSORS(...)
#endif

/*==================[internal data definition]===============================*/

CCS811 ccs811;
Adafruit_BMP280 bmp280(&Wire);
ClosedCube_HDC1080 hdc1080;
Bsec bme680;
MHZ19 mhz19;
//SoftwareSerial mySerial(SER_RX, SER_TX);
SoftwareSerial mySerial;

/*==================[internal functions declaration]=========================*/
MySensors::MySensors()
{
	initialized = false;
}

/* init */
bool MySensors::init(uint8_t mode)
{
	bool ret = false;
	if (initialized)
		return true;

	if ((mode == MYSENSORS_MODE_AUTO && !ret) || mode == MYSENSORS_MODE_MHZ19)
	{
		DEBUG_MYSENSORS("Wire end returns: %d\n", ret);
		ret = initMHZ19();
		if (ret)
			currentMode = MYSENSORS_MODE_MHZ19;
	}

		if ((mode == MYSENSORS_MODE_AUTO  && !ret) || mode == MYSENSORS_MODE_CCS811)
	{
		Wire.begin(I2C_SDA, I2C_SCL);
		ret = initCCS811();
		if (ret)
			currentMode = MYSENSORS_MODE_CCS811;
	}

	if ((mode == MYSENSORS_MODE_AUTO && !ret) || mode == MYSENSORS_MODE_BME680)
	{
		ret = initBME680();
		if (ret)
			currentMode = MYSENSORS_MODE_BME680;
	}

	initialized = ret;
	return ret;
}

bool MySensors::initCCS811()
{
	bool ret;

	// This setup routine is similar to what is used in the subclass' .begin() function
	ret = ccs811.begin();
	if (ret)
	{
		ret = ccs811.start(CCS811_MODE_1SEC);
		if (ret)
		{
			DEBUG_MYSENSORS("CCS811 init success\n");
		}
		else
		{
			DEBUG_MYSENSORS("CCS811 configuration failed\n");
		}
	}
	else
	{
		ret = false;
		DEBUG_MYSENSORS("CCS811 init failed\n");
	}

	/* if CCS811 detected continue looking */
	if (ret)
	{
		if (bmp280.begin(BMP280_ADDRESS_ALT))
		{
			/* Default settings from datasheet. */
			bmp280.setSampling(Adafruit_BMP280::MODE_NORMAL,	  /* Operating Mode. */
							Adafruit_BMP280::SAMPLING_X2,	  /* Temp. oversampling */
							Adafruit_BMP280::SAMPLING_X16,	  /* Pressure oversampling */
							Adafruit_BMP280::FILTER_X16,	  /* Filtering. */
							Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
			DEBUG_MYSENSORS("BMP280 init success\n");
		}
		else
		{
			DEBUG_MYSENSORS("BMP280 init failed\n");
			ret = 0;
		}

		hdc1080.begin(HDC1080_ADDR);
		if (hdc1080.readManufacturerId() != 0x5449)
		{
			ret = 0;
			DEBUG_MYSENSORS("HDC1080 init failed\n");
		}
		else
		{
			DEBUG_MYSENSORS("HDC1080 init success\n");
		}
	}
	return ret;
}

bool MySensors::initBME680()
{
	bme680.begin(BME680_ADDR, Wire);

	checkBME680();

	bme680.setTemperatureOffset(5);
	bsec_virtual_sensor_t sensorList[10] = {
		BSEC_OUTPUT_RAW_TEMPERATURE,
		BSEC_OUTPUT_RAW_PRESSURE,
		BSEC_OUTPUT_RAW_HUMIDITY,
		BSEC_OUTPUT_RAW_GAS,
		BSEC_OUTPUT_IAQ,
		BSEC_OUTPUT_STATIC_IAQ,
		BSEC_OUTPUT_CO2_EQUIVALENT,
		BSEC_OUTPUT_BREATH_VOC_EQUIVALENT,
		BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
		BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
	};

	// bme680.updateSubscription(sensorList, 10, BSEC_SAMPLE_RATE_LP);
	bme680.updateSubscription(sensorList, 10, BSEC_SAMPLE_RATE_CONTINUOUS);

	return checkBME680();
}

bool MySensors::checkBME680()
{
	bool ret = true;
	if (bme680.status != BSEC_OK)
	{
		if (bme680.status < BSEC_OK)
		{
			DEBUG_MYSENSORS("BSEC error code: %d\n", bme680.status);
		}
		else
		{
			DEBUG_MYSENSORS("BSEC warning code: %d\n", bme680.status);
		}
		ret = false;
	}

	if (bme680.bme680Status != BME680_OK)
	{
		if (bme680.bme680Status < BME680_OK)
		{
			DEBUG_MYSENSORS("BME680 error code: %d\n", bme680.bme680Status);
		}
		else
		{
			DEBUG_MYSENSORS("BME680 warning code: %d\n", bme680.bme680Status);
		}
		ret = false;
	}

	return ret;
}

bool MySensors::initMHZ19()
{
	bool ret = false;
	int co2;
    mySerial.begin(9600, SWSERIAL_8N1, SER_RX, SER_TX);
    mhz19.begin(mySerial);
	mhz19.autoCalibration(true);
	co2 = mhz19.getCO2();

	if (mhz19.errorCode == RESULT_OK)
	{
		DEBUG_MYSENSORS("MH-Z19 init success %d\n", co2);
		ret = true;
	}
	else
	{
		DEBUG_MYSENSORS("MH-Z19 init failed %d\n", co2);
		mySerial.end();
	}
	return ret;
}

bool MySensors::update()
{
	static TickType_t xLastWakeTime = xTaskGetTickCount();
	bool ret = false;
	if (currentMode == MYSENSORS_MODE_CCS811)
	{
		// check to see if data is ready with .dataAvailable()
		// ccs811 is the slower sensor
		uint16_t errstat, raw;

		/* block if no data */
		while (1)
		{
			ccs811.read(&co2, &tvoc, &errstat, &raw);
			if (errstat == CCS811_ERRSTAT_OK)
			{
				// Initialise the xLastWakeTime variable with the current time.
				xLastWakeTime = xTaskGetTickCount();

				// request BMP280 measurement
				pressure = bmp280.readPressure();
				temperature = bmp280.readTemperature();

				// read hdc1080 measurement
				// DEBUG_MYSENSORS("TIME:%lu hdcTemp:%f bmpTemp:%f\n", float(hdc1080.readTemperature()), temperature )
				// temperature = float(hdc1080.readTemperature());
				humidity = float(hdc1080.readHumidity());

				// Set HUMID/TEMP for compensation
				ccs811.set_envdata_Celsius_percRH(temperature, humidity);

				DEBUG_MYSENSORS("TIME:%lu CO2:%u TVOC:%u humidity:%f temperature:%f pressure:%f\n", millis(), co2, tvoc, humidity, temperature, pressure);
				return true;
			}
			else if (errstat == CCS811_ERRSTAT_OK_NODATA)
			{
				vTaskDelayUntil(&xLastWakeTime, 100 / portTICK_PERIOD_MS);
			}
			else
				break;
		}
		DEBUG_MYSENSORS("CCS811 read ERROR\n");
		return false;
	}
	if (currentMode == MYSENSORS_MODE_BME680)
	{
		while (1)
		{
			if (bme680.run())
			{
				xLastWakeTime = xTaskGetTickCount();
				// If new data is available
				// bme680.rawTemperature;
				pressure = bme680.pressure;
				// bme680.rawHumidity;
				// bme680.gasResistance;
				iaq = bme680.iaq;
				iaqAccuracy = bme680.iaqAccuracy;
				temperature = bme680.temperature;
				humidity = bme680.humidity;
				// bme680.staticIaq;
				co2 = bme680.co2Equivalent;
				tvoc = bme680.breathVocEquivalent;
				DEBUG_MYSENSORS("BME680 TIME:%lu nextCall:%" PRId64 " IAQ:%f, IAQaccuracy:%u\n", millis(), bme680.nextCall, bme680.iaq, bme680.iaqAccuracy);
				DEBUG_MYSENSORS("BME680 TIME:%lu CO2:%f CO2Accuracy:%u TVOC:%f humidity:%f temperature:%f pressure:%f\n", millis(), bme680.co2Equivalent, bme680.co2Accuracy, bme680.breathVocEquivalent, humidity, temperature, pressure);
				return true;
			}
			else
			{
				vTaskDelayUntil(&xLastWakeTime, 100 / portTICK_PERIOD_MS);
			}
		}
	}
	if (currentMode == MYSENSORS_MODE_MHZ19)
	{
			int tmpCo2;
			float tmpTemperature;
			vTaskDelayUntil(&xLastWakeTime, 1000 / portTICK_PERIOD_MS);
			tmpCo2 = mhz19.getCO2();
			if (mhz19.errorCode == RESULT_OK && tmpCo2 != 0) {
				tmpTemperature = mhz19.getTemperature(true);
				if (mhz19.errorCode == RESULT_OK) {
					co2 = tmpCo2;
					temperature = tmpTemperature;
					ret = true;
					DEBUG_MYSENSORS("MHZ19 TIME:%lu, CO2:%u, Temperature:%f\n", millis(), co2, temperature);
				}
			}
			if (!ret) {
				DEBUG_MYSENSORS("Error tomando muestra\n");
			}
			xLastWakeTime = xTaskGetTickCount();
			return ret;
	}
	return false;
}

uint16_t MySensors::getCO2()
{
	return co2;
}

uint16_t MySensors::getTVOC()
{
	return tvoc;
}

float MySensors::getTemp()
{
	// return float(hdcTemp);
	return temperature;
}

float MySensors::getHum()
{
	return humidity;
}

float MySensors::getPres()
{
	return pressure;
}

float MySensors::getIaq()
{
	return iaq;
}

uint8_t MySensors::getIaqAccuracy()
{
	return iaqAccuracy;
}

MySensors sensors;
