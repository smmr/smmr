/******************************************************************************

  Sebastián Guarino

  Distributed as-is; no warranty is given.
******************************************************************************/

#ifndef __MySD_h__
#define __MySD_h__

#include <Arduino.h>
#include <FS.h>

#define MYSD_SDMMC  1

#if MYSD_SDMMC==1
#include <SD_MMC.h>
#define MYSD_SD SD_MMC
#endif
#if MYSD_SDMMC==0
#include <SD.h>
#define MYSD_SD SD
#endif
#include <SPI.h>

const char C_STATUS_EMPTY[] = "Vacia";
const char C_STATUS_PRESENT[] = "Presente";

const char C_MYSD_FILENAME[] PROGMEM = "/output.txt";



class MySD
{
  public:
    bool init();
    void mount();
    String status;

    bool removeFile(const char *filename = C_MYSD_FILENAME);
    bool appendLine(const String &data, const char *filename = C_MYSD_FILENAME);
    bool readLine(String &line, const char *filename = C_MYSD_FILENAME);

    String getDir(const String &dirname);
    String getTotalBytes();
    String getUsedBytes();
    String getFreeBytes();

  private:
    String uint64ToString(uint64_t input);
    File outputFile;
};

extern MySD sd;

#endif
