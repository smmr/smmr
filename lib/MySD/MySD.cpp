/******************************************************************************

  Sebastián Guarino

  Distributed as-is; no warranty is given.
******************************************************************************/

#include <MySD.h>

#if DEBUG
  #ifndef DEBUG_ESP_PORT
    #define DEBUG_ESP_PORT Serial
  #endif
  #ifndef DEBUG_ESP_WIFI
    #define DEBUG_ESP_WIFI 1
  #endif
  #define DEBUG_MYSD(...) DEBUG_ESP_PORT.print("DEBUG mySD: "); DEBUG_ESP_PORT.printf( __VA_ARGS__ )
#else
  #define DEBUG_MYSD(...)
#endif

/* init */
bool MySD::init() {
	/* initialize object members with defaults */
	bool ret;

	/* GPIO_NUM_0 is shorted with GPIO_NUM_2 (DATA0) */
	/* We leave it in high impedance state */
	pinMode(GPIO_NUM_0, OPEN_DRAIN);

	ret = MYSD_SD.begin();

	/* Optionaly you can select SD.begin(CS_PIN) */
    if(!ret) {
    	status = C_STATUS_EMPTY;
        DEBUG_MYSD("Card Mount Failed");
        return false;
    }

    status = C_STATUS_PRESENT;
    return true;
}

bool MySD::removeFile(const char *filename) {
	return MYSD_SD.remove(filename);
}

bool MySD::appendLine(const String &data, const char *filename) {
	if (!outputFile) {
		if (!MYSD_SD.exists(filename)) {
			outputFile = MYSD_SD.open(filename, FILE_WRITE);
			DEBUG_MYSD("Open file %s in write mode\n", filename);
		} else {
			outputFile = MYSD_SD.open(filename, FILE_APPEND);
			DEBUG_MYSD("Open file %s in append mode\n", filename);
		}
		if (!outputFile) {
			DEBUG_MYSD("Error: opening Open file %s\n", filename);
			return false;
		}
	}

	if (outputFile.print(data) != data.length()) {
		DEBUG_MYSD("Error: written less bytes than expected\n");
		return false;
	}
	// TODO research bug in SD library. Close shouldn't be necesary
	// better fix would be count N writes to close and reopen
	outputFile.close();
	DEBUG_MYSD("Write OK\n");
	return true;
}

bool MySD::readLine(String &line, const char *filename) {
	String tmp;

	if (!MYSD_SD.exists(filename)) {
		return false;
	}

	if (!outputFile) {
		outputFile = MYSD_SD.open(filename, FILE_READ);
	}

	if (!outputFile) {
			return false;
	}

	line = outputFile.readStringUntil('\n');

	// TODO investigate bug in SD library. Close shouldn't be necesary
	// or count N writes to close and reopen
	outputFile.close();
	return true;
}

String MySD::getDir(const String &dirname) {
	String listing;
	File root = MYSD_SD.open(dirname);
	if (!root.isDirectory())
		return "";

	if (!root.isDirectory()) {
		return "";
	}

	listing.concat("MEMORIA");
	File file = root.openNextFile();
	while (file) {
		if (!file.isDirectory()) {
			listing.concat("&/");
			listing.concat(file.name());
			listing.concat(",");
			listing.concat(file.size());
			listing.concat(",");
			// TODO DATE/TIME here
			// must be implemented inside FS library
			listing.concat("");
		}
		file = root.openNextFile();
	}
	return listing;
}

String MySD::getUsedBytes() {
	uint64_t u = MYSD_SD.usedBytes();
	return uint64ToString(u);
}

String MySD::getTotalBytes() {
	uint64_t u = MYSD_SD.totalBytes();
	return uint64ToString(u);
}

String MySD::getFreeBytes() {
	uint64_t u = MYSD_SD.totalBytes() - MYSD_SD.usedBytes();
	return uint64ToString(u);
}

String MySD::uint64ToString(uint64_t input) {
	String result = "";
	uint8_t base = 10;

	do {
		char c = input % base;
		input /= base;

		if (c < 10)
			c +='0';
		else
			c += 'A' - 10;
		result = c + result;
	} while (input);
	return result;
}

MySD sd;

