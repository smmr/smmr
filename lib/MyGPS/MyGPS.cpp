/******************************************************************************

  Sebastián Guarino
  https://gitlab.com/sguarin/smmr.git

  Distributed as-is; no warranty is given.
******************************************************************************/

/*==================[inclusions]=============================================*/
#include "MyGPS.h"

/*==================[macros and definitions]=================================*/

#if DEBUG
  #ifndef DEBUG_ESP_PORT
    #define DEBUG_ESP_PORT Serial
  #endif
  #ifndef DEBUG_ESP_WIFI
    #define DEBUG_ESP_WIFI 1
  #endif
  #define DEBUG_MYGPS(...) DEBUG_ESP_PORT.print("DEBUG MYGPS: "); DEBUG_ESP_PORT.printf( __VA_ARGS__ )
#else
  #define DEBUG_MYGPS(...)
#endif

#define MYGPS_SERIAL Serial2
/*==================[internal data declaration]==============================*/

MyGPS gps(MYGPS_SERIAL);

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void MyGPS::myGPSTask(void *pvParameters)
{
	DEBUG_MYGPS("Starting myGPSTask\n");
	MyGPS* p = static_cast<MyGPS *>(pvParameters);
	
	while (true)
	{
		if (p->gpsSerial.available())
			do {} while (xSemaphoreTake(p->lock, portMAX_DELAY) != pdPASS);
		do 
		{
			p->tinyGps.encode(p->gpsSerial.read());
			
		} while (p->gpsSerial.available());
		xSemaphoreGive(p->lock);
		vTaskDelay(50 / portTICK_PERIOD_MS);
	}
}

/* Constructor */
MyGPS::MyGPS(HardwareSerial &newGpsSerial) : gpsSerial(newGpsSerial) {
	maxAge = UINT32_MAX;
	myGPSTaskHandle = NULL;
}

void MyGPS::setMaxAge(const uint32_t newMaxAge) {
	maxAge = newMaxAge;
}

bool MyGPS::begin() {
	bool ret = true;
	bool gpsPresent = false;

	unsigned long u = millis();
	gpsSerial.begin(9600);
	gpsSerial.setTimeout(2000);

	// create serial mutex
	lock = xSemaphoreCreateMutex();
	if (lock == 0) {
		//INFO_MYGPS("INFO MAIN: Error creating serial mutex\n");
		return false;
	}

	// While not timed out and not found yet
	while ((millis() - u < 2000) && !gpsPresent) {
		if (gpsSerial.available()) {
		    if (tinyGps.encode(gpsSerial.read())) {
				gpsPresent = true;
			}
		}
	}
	if (!gpsPresent) {
		return false;
	}

	// create serial task
	xTaskCreatePinnedToCore(
			myGPSTask,			/* Function to implement the task */
			"myGPSTask",		/* Name of the task */
			10000,				/* Stack size in words */
			this,				/* Task input parameter */
			2,					/* Priority of the task */
			&myGPSTaskHandle, 	/* Task handle. */
			1);					/* Core where the task should run */

	return ret;
}

void MyGPS::end() {
	if (myGPSTaskHandle) {
		vTaskDelete(myGPSTaskHandle);
		myGPSTaskHandle = NULL;
	}
	if (lock) {
		vSemaphoreDelete(lock);
		lock = NULL;
	}
}

String MyGPS::getStatusStr() {
	String ret = "Online";
	if (!tinyGps.satellites.isValid()) {
		ret = "Error";
	}
	if (tinyGps.satellites.age() > maxAge) {
		ret = "Stalled";
	}

	return ret;
}

String MyGPS::getTimeStr() {
	String time = "";

	if (tinyGps.date.isValid() && tinyGps.date.age() < maxAge && tinyGps.time.isValid() && tinyGps.time.age() < maxAge)
	{
		char buf[1 + 3 * sizeof(uint32_t)];
		xSemaphoreTake(lock, portMAX_DELAY);
		snprintf(buf, sizeof(buf), "%u", tinyGps.date.year());
		time.concat(buf);
		time.concat("-");
		snprintf(buf, sizeof(buf), "%02u", tinyGps.date.month());
		time.concat(buf);
		time.concat("-");
		snprintf(buf, sizeof(buf), "%02u", tinyGps.date.day());
		time.concat(buf);
		
		time.concat(" ");
		snprintf(buf, sizeof(buf), "%02u", tinyGps.time.hour());
		time.concat(buf);
		time.concat(":");
		snprintf(buf, sizeof(buf), "%02u", tinyGps.time.minute());
		time.concat(buf);
		time.concat(":");
		snprintf(buf, sizeof(buf), "%02u", tinyGps.time.second());
		time.concat(buf);
		xSemaphoreGive(lock);
	}
	return time;
}

double MyGPS::getLat() {
	return (tinyGps.location.age() < maxAge) ? tinyGps.location.lat() : 0;
}

double MyGPS::getLng() {
	return (tinyGps.location.age() < maxAge) ?  tinyGps.location.lng() : 0;
}

double MyGPS::getAlt() {
	return (tinyGps.location.age() < maxAge) ? tinyGps.altitude.meters() : 0;
}

int MyGPS::getSatellites() {
	return (tinyGps.satellites.age() < maxAge) ? tinyGps.satellites.value() : -1;
}

/*==================[end of file]============================================*/