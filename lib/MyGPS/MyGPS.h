/******************************************************************************

  Sebastián Guarino
  https://gitlab.com/sguarin/smmr.git

  Distributed as-is; no warranty is given.
******************************************************************************/

#ifndef _MYGPS_H_
#define _MYGPS_H_

#include <Arduino.h>
#include <TinyGPS++.h>

class MyGPS
{
  public:
    MyGPS(HardwareSerial &newGpsSerial);
    bool begin();
    void setMaxAge(const uint32_t newMaxAge);
    void end();

    String getStatusStr();
    String getTimeStr();
    double getLat();
    double getLng();
    double getAlt();
    int getSatellites();

  protected:
    TaskHandle_t myGPSTaskHandle;
    static void myGPSTask(void *pvParameters);
    
  private:
    uint32_t maxAge;
    TinyGPSPlus tinyGps;
    HardwareSerial &gpsSerial;
    SemaphoreHandle_t lock;
};

extern MyGPS gps;

#endif
