/******************************************************************************

  Sebastián Guarino

  Distributed as-is; no warranty is given.
******************************************************************************/

#include <SystemConfig.h>
#if SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_FS
#include <FS.h>
#ifdef ESP32
#include <SPIFFS.h>
#endif
#endif

#if SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_PREFERENCES
#include <Preferences.h>
#endif

/*==================[macros and definitions]=================================*/

#if DEBUG
#ifndef DEBUG_ESP_PORT
#define DEBUG_ESP_PORT Serial
#endif
#ifndef DEBUG_ESP_WIFI
#define DEBUG_ESP_WIFI 1
#endif
#define DEBUG_SYSTEMCONFIG(...)                   \
	DEBUG_ESP_PORT.print("DEBUG SystemConfig: "); \
	DEBUG_ESP_PORT.printf(__VA_ARGS__)
#else
#define DEBUG_SYSTEMCONFIG(...)
#endif

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_SYSTEMCONFIG(...)                  \
	INFO_ESP_PORT.print("INFO SystemConfig: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

#ifndef C_SYSTEM
#ifdef GC_SYSTEM_MODEL_VALUE
#define C_SYSTEM GC_SYSTEM_MODEL_VALUE
#else
#define C_SYSTEM "smmr"
#endif
#endif
#ifndef C_SYSTEM_SECRET
#define C_SYSTEM_SECRET "smmr0000"
#endif

/*==================[internal data definition]===============================*/

SystemConfig config;
#if SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_PREFERENCES
Preferences prefs;
#endif

/*==================[internal functions declaration]=========================*/

/*==================[internal functions definition]==========================*/

/* Constructor */

SystemConfig::SystemConfig()
{
	/* initialize object members with defaults */
	loadDefaults();
}

void SystemConfig::loadDefaults()
{

	systemConfigList[SC_WIFI_SSID].key = C_WIFI_SSID;
	systemConfigList[SC_WIFI_SSID].keylen = sizeof(C_WIFI_SSID) - 1;
	systemConfigList[SC_WIFI_SSID].value = C_SYSTEM;

	systemConfigList[SC_WIFI_SECRET].key = C_WIFI_SECRET;
	systemConfigList[SC_WIFI_SECRET].keylen = sizeof(C_WIFI_SECRET) - 1;
	systemConfigList[SC_WIFI_SECRET].value = C_SYSTEM_SECRET;

	systemConfigList[SC_WIFI_CHANNEL].key = C_WIFI_CHANNEL;
	systemConfigList[SC_WIFI_CHANNEL].keylen = sizeof(C_WIFI_CHANNEL) - 1;
	systemConfigList[SC_WIFI_CHANNEL].value = "5";

	systemConfigList[SC_WIFI_HIDDEN].key = C_WIFI_HIDDEN;
	systemConfigList[SC_WIFI_HIDDEN].keylen = sizeof(C_WIFI_HIDDEN) - 1;
	systemConfigList[SC_WIFI_HIDDEN].value = "0";

	systemConfigList[SC_WIFI_C_ENABLE].key = C_WIFI_C_ENABLE;
	systemConfigList[SC_WIFI_C_ENABLE].keylen = sizeof(C_WIFI_C_ENABLE) - 1;
	systemConfigList[SC_WIFI_C_ENABLE].value = "0";

	systemConfigList[SC_WIFI_C_SSID].key = C_WIFI_C_SSID;
	systemConfigList[SC_WIFI_C_SSID].keylen = sizeof(C_WIFI_C_SSID) - 1;
	systemConfigList[SC_WIFI_C_SSID].value = "";

	systemConfigList[SC_WIFI_C_SECRET].key = C_WIFI_C_SECRET;
	systemConfigList[SC_WIFI_C_SECRET].keylen = sizeof(C_WIFI_C_SECRET) - 1;
	systemConfigList[SC_WIFI_C_SECRET].value = "";

	systemConfigList[SC_MEM_ENABLE].key = C_MEM_ENABLE;
	systemConfigList[SC_MEM_ENABLE].keylen = sizeof(C_MEM_ENABLE) - 1;
	systemConfigList[SC_MEM_ENABLE].value = "0";

	systemConfigList[SC_GPS_MODE].key = C_GPS_MODE;
	systemConfigList[SC_GPS_MODE].keylen = sizeof(C_GPS_MODE) - 1;
	systemConfigList[SC_GPS_MODE].value = "";

	systemConfigList[SC_GPS_RATE].key = C_GPS_RATE;
	systemConfigList[SC_GPS_RATE].keylen = sizeof(C_GPS_RATE) - 1;
	systemConfigList[SC_GPS_RATE].value = "1";

	systemConfigList[SC_SENSOR_CO2].key = C_SENSOR_CO2;
	systemConfigList[SC_SENSOR_CO2].keylen = sizeof(C_SENSOR_CO2) - 1;
	systemConfigList[SC_SENSOR_CO2].value = "0";

	systemConfigList[SC_A1_ENABLE].key = C_A1_ENABLE;
	systemConfigList[SC_A1_ENABLE].keylen = sizeof(C_A1_ENABLE) - 1;
	systemConfigList[SC_A1_ENABLE].value = "0";

	systemConfigList[SC_A2_ENABLE].key = C_A2_ENABLE;
	systemConfigList[SC_A2_ENABLE].keylen = sizeof(C_A2_ENABLE) - 1;
	systemConfigList[SC_A2_ENABLE].value = "0";

	systemConfigList[SC_D1_ENABLE].key = C_D1_ENABLE;
	systemConfigList[SC_D1_ENABLE].keylen = sizeof(C_D1_ENABLE) - 1;
	systemConfigList[SC_D1_ENABLE].value = "0";

	systemConfigList[SC_D2_ENABLE].key = C_D2_ENABLE;
	systemConfigList[SC_D2_ENABLE].keylen = sizeof(C_D2_ENABLE) - 1;
	systemConfigList[SC_D2_ENABLE].value = "0";

	systemConfigList[SC_D3_ENABLE].key = C_D3_ENABLE;
	systemConfigList[SC_D3_ENABLE].keylen = sizeof(C_D3_ENABLE) - 1;
	systemConfigList[SC_D3_ENABLE].value = "0";

	systemConfigList[SC_D4_ENABLE].key = C_D4_ENABLE;
	systemConfigList[SC_D4_ENABLE].keylen = sizeof(C_D4_ENABLE) - 1;
	systemConfigList[SC_D4_ENABLE].value = "0";

	systemConfigList[SC_INPUTS_RATE].key = C_INPUTS_RATE;
	systemConfigList[SC_INPUTS_RATE].keylen = sizeof(C_INPUTS_RATE) - 1;
	systemConfigList[SC_INPUTS_RATE].value = "1";

	systemConfigList[SC_SERVER_MODE].key = C_SERVER_MODE;
	systemConfigList[SC_SERVER_MODE].keylen = sizeof(C_SERVER_MODE) - 1;
	systemConfigList[SC_SERVER_MODE].value = "";

	systemConfigList[SC_SERVER_HOST].key = C_SERVER_HOST;
	systemConfigList[SC_SERVER_HOST].keylen = sizeof(C_SERVER_HOST) - 1;
	systemConfigList[SC_SERVER_HOST].value = "";

	systemConfigList[SC_SERVER_TOPIC].key = C_SERVER_TOPIC;
	systemConfigList[SC_SERVER_TOPIC].keylen = sizeof(C_SERVER_TOPIC) - 1;
	systemConfigList[SC_SERVER_TOPIC].value = "";

	systemConfigList[SC_SERVER_USER].key = C_SERVER_USER;
	systemConfigList[SC_SERVER_USER].keylen = sizeof(C_SERVER_USER) - 1;
	systemConfigList[SC_SERVER_USER].value = "";

	systemConfigList[SC_SERVER_PASSWORD].key = C_SERVER_PASSWORD;
	systemConfigList[SC_SERVER_PASSWORD].keylen = sizeof(C_SERVER_PASSWORD) - 1;
	systemConfigList[SC_SERVER_PASSWORD].value = "";

	systemConfigList[SC_SERVER_COMMANDS].key = C_SERVER_COMMANDS;
	systemConfigList[SC_SERVER_COMMANDS].keylen = sizeof(C_SERVER_COMMANDS) - 1;
	systemConfigList[SC_SERVER_COMMANDS].value = "0";

	systemConfigList[SC_UPDATE_FS_VER].key = C_UPDATE_FS_VER;
	systemConfigList[SC_UPDATE_FS_VER].keylen = sizeof(C_UPDATE_FS_VER) - 1;
	systemConfigList[SC_UPDATE_FS_VER].value = "";

	systemConfigList[SC_UPDATE_FW_VER].key = C_UPDATE_FW_VER;
	systemConfigList[SC_UPDATE_FW_VER].keylen = sizeof(C_UPDATE_FW_VER) - 1;
	systemConfigList[SC_UPDATE_FW_VER].value = "";

	systemConfigList[SC_UPDATE_FS].key = C_UPDATE_FS;
	systemConfigList[SC_UPDATE_FS].keylen = sizeof(C_UPDATE_FS) - 1;
	systemConfigList[SC_UPDATE_FS].value = "";

	systemConfigList[SC_UPDATE_FW].key = C_UPDATE_FW;
	systemConfigList[SC_UPDATE_FW].keylen = sizeof(C_UPDATE_FW) - 1;
	systemConfigList[SC_UPDATE_FW].value = "";
}

bool SystemConfig::begin()
{
	bool ret;

/* FS start */
#if SYSTEM_CONFIG_STORAGE == FS
	ret = SPIFFS.begin();
	if (ret)
	{
		DEBUG_SYSTEMCONFIG("Mounting SPIFFS: OK\n");
	}
	else
	{
		INFO_SYSTEMCONFIG("ERROR: could not mount SPIFFS\n");
		return false;
	}

	if (!SPIFFS.exists(SYSTEM_CONFIG_FILENAME))
	{
		DEBUG_SYSTEMCONFIG("Creating first time config\n");
		return writeConfig();
	}
	else
	{
		return loadConfig();
	}
#elif SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_PREFERENCES

	ret = prefs.begin(SYSTEM_CONFIG_NAME, false);
	if (ret)
	{
		DEBUG_SYSTEMCONFIG("Reading Preferences: OK\n");
	}
	else
	{
		INFO_SYSTEMCONFIG("ERROR: could not read Preferences\n");
		return false;
	}
	if (prefs.getString(systemConfigList[SC_WIFI_SSID].key).isEmpty())
	{
		DEBUG_SYSTEMCONFIG("Creating first time config\n");
		return writeConfig();
	}
	else
	{
		return loadConfig();
	}
#endif
}

void SystemConfig::end()
{
#if SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_FS
	SPIFFS.end();
#elif SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_PREFERENCES
	prefs.end();
#endif
}

bool SystemConfig::resetConfig()
{
	DEBUG_SYSTEMCONFIG("Reseting config\n");
	/* Remove /config */
#if SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_FS
	if (SPIFFS.exists(SYSTEM_CONFIG_FILENAME))
	{
		if (!SPIFFS.remove(SYSTEM_CONFIG_FILENAME))
		{
			INFO_SYSTEMCONFIG("ERROR: cannot remove config\n");
		}
	}

#elif SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_PREFERENCES
	/* Clear Preferences */
	if (!prefs.clear())
	{
		INFO_SYSTEMCONFIG("ERROR: cannot remove config\n");
	}
#endif
	loadDefaults();
	return writeConfig();
}

const char *SystemConfig::getValue(uint8_t key)
{
	return systemConfigList[key].value.c_str();
}

const char *SystemConfig::getValue(const char *key)
{
	for (uint8_t u = 0; u < SYSTEMCONFIGLIST_SIZE; u++)
	{
		if (!strcmp(key, systemConfigList[u].key))
			return getValue(u);
	}
	return NULL;
}

const char *SystemConfig::getValue(const String &key)
{
	for (uint8_t u = 0; u < SYSTEMCONFIGLIST_SIZE; u++)
	{
		if (key.equals(systemConfigList[u].key))
			return getValue(u);
	}
	return NULL;
}

String SystemConfig::getValueStr(uint8_t key)
{
	return systemConfigList[key].value;
}

String SystemConfig::getValueStr(const char *key)
{
	for (uint8_t u = 0; u < SYSTEMCONFIGLIST_SIZE; u++)
	{
		if (!strcmp(key, systemConfigList[u].key))
			return systemConfigList[u].value;
	}
	return "";
}

String SystemConfig::getValueStr(const String &key)
{
	for (uint8_t u = 0; u < SYSTEMCONFIGLIST_SIZE; u++)
	{
		if (key.equals(systemConfigList[u].key))
			return systemConfigList[u].value;
	}
	return "";
}

bool SystemConfig::setValue(uint8_t key, String &value)
{
	if (validKeyValue(key, value))
	{
		DEBUG_SYSTEMCONFIG("setConfig Changing %s parameter to %s\n", systemConfigList[key].key, value.c_str());
		systemConfigList[key].value = value;
		return true;
	}
	return false;
}

bool SystemConfig::setValue(uint8_t key, const char *pvalue)
{
	String value = pvalue;
	if (validKeyValue(key, value))
	{
		DEBUG_SYSTEMCONFIG("setConfig Changing %s parameter to %s\n", systemConfigList[key].key, pvalue);
		systemConfigList[key].value = value;
		return true;
	}
	return false;
}

bool SystemConfig::setValue(const String &key, const String &new_value)
{
	for (uint8_t u = 0; u < SYSTEMCONFIGLIST_SIZE; u++)
	{
		if (key.equals(systemConfigList[u].key))
		{
			if (validKeyValue(u, new_value))
			{
				if (!systemConfigList[u].value.equals(new_value))
				{
					DEBUG_SYSTEMCONFIG("setConfig Changing %s parameter to %s\n", key.c_str(), new_value.c_str());
					systemConfigList[u].value = new_value;
					writeConfig();
				}
				return true;
			}
		}
	}
	return false;
}

bool SystemConfig::setValue(const String &key, const char *new_value)
{
	for (uint8_t u = 0; u < SYSTEMCONFIGLIST_SIZE; u++)
	{
		if (validKeyValue(u, new_value))
		{
			if (key.equals(systemConfigList[u].key))
			{
				if (!systemConfigList[u].value.equals(new_value))
				{
					DEBUG_SYSTEMCONFIG("setConfig Changing %s parameter to %s\n", key.c_str(), new_value);
					systemConfigList[u].value = new_value;
					writeConfig();
				}
				return true;
			}
		}
	}
	return false;
}

bool SystemConfig::validKeyValue(uint8_t key, const String &value)
{
	if (key >= SYSTEMCONFIGLIST_SIZE)
		return false;
	switch (key)
	{
	case SC_WIFI_SSID:
	case SC_WIFI_C_SSID:
	case SC_SERVER_HOST:
	case SC_SERVER_USER:
	case SC_SERVER_TOPIC:
	case SC_SERVER_PASSWORD:
	case SC_UPDATE_FS_VER:
	case SC_UPDATE_FW_VER:
	case SC_UPDATE_FS:
	case SC_UPDATE_FW:
		return validSsid(value);

	case SC_WIFI_SECRET:
	case SC_WIFI_C_SECRET:
		return validSecret(value);

	case SC_WIFI_CHANNEL:
		return validChannel(value);

	case SC_WIFI_HIDDEN:
	case SC_WIFI_C_ENABLE:
	case SC_SENSOR_CO2:
	case SC_MEM_ENABLE:
	case SC_A1_ENABLE:
	case SC_A2_ENABLE:
	case SC_D1_ENABLE:
	case SC_D2_ENABLE:
	case SC_D3_ENABLE:
	case SC_D4_ENABLE:
	case SC_SERVER_COMMANDS:
		return validEnable(value);

	case SC_INPUTS_RATE:
		return validInputsRate(value);

	case SC_SERVER_MODE:
		return validServerMode(value);

	case SC_GPS_RATE:
		return validGpsRate(value);

	case SC_GPS_MODE:
		return validGpsMode(value);

	default:
		return false;
	}
}

bool SystemConfig::setWifiSsid(const String &new_wifi_ssid)
{
	if (validSsid(new_wifi_ssid))
	{
		if (new_wifi_ssid != systemConfigList[SC_WIFI_SSID].value)
		{
			DEBUG_SYSTEMCONFIG("Changing wifi_ssid parameter\n");
			systemConfigList[SC_WIFI_SSID].value = new_wifi_ssid;
			writeConfig();
		}
		return true;
	}
	return false;
}

bool SystemConfig::setWifiSecret(const String &new_wifi_secret)
{
	if (validSecret(new_wifi_secret))
	{
		if (new_wifi_secret != systemConfigList[SC_WIFI_SECRET].value)
		{
			DEBUG_SYSTEMCONFIG("Changing wifi_secret parameter\n");
			systemConfigList[SC_WIFI_SECRET].value = new_wifi_secret;
			writeConfig();
		}
		return true;
	}
	return false;
}

bool SystemConfig::setWifiChannel(const String &new_wifi_channel)
{
	//if (validChannel(newchannel)) {
	if (new_wifi_channel != systemConfigList[SC_WIFI_CHANNEL].value)
	{
		DEBUG_SYSTEMCONFIG("Changing wifi_channel parameter\n");
		systemConfigList[SC_WIFI_CHANNEL].value = new_wifi_channel;

		writeConfig();
	}
	return true;
	//}
	//return false;
}

bool SystemConfig::setWifi(const String &new_wifi_ssid, const String &new_wifi_secret, const String &new_wifi_channel, const String &new_wifi_hidden)
{
	if (validSsid(new_wifi_ssid) && validSecret(new_wifi_secret) && validChannel(new_wifi_channel) && validEnable(new_wifi_hidden))
	{
		if (new_wifi_ssid != systemConfigList[SC_WIFI_SSID].value || new_wifi_secret != systemConfigList[SC_WIFI_SECRET].value || new_wifi_channel != systemConfigList[SC_WIFI_CHANNEL].value)
		{
			DEBUG_SYSTEMCONFIG("Changing WIFI parameters\n");
			systemConfigList[SC_WIFI_SSID].value = new_wifi_ssid;
			systemConfigList[SC_WIFI_SECRET].value = new_wifi_secret;
			systemConfigList[SC_WIFI_CHANNEL].value = new_wifi_channel;
			systemConfigList[SC_WIFI_HIDDEN].value = new_wifi_hidden;
			writeConfig();
		}
		return true;
	}
	DEBUG_SYSTEMCONFIG("NOT changing WIFI parameters\n");
	return false;
}

bool SystemConfig::setControlWifi(const String &newwifi_c_enable, const String &newwifi_c_ssid, const String &newwifi_c_secret)
{
	if ((newwifi_c_enable.equals("0")) || (newwifi_c_enable.equals("1") && validSsid(newwifi_c_ssid) && validSecret(newwifi_c_secret)))
	{
		if (newwifi_c_enable != systemConfigList[SC_WIFI_C_ENABLE].value || newwifi_c_ssid != systemConfigList[SC_WIFI_C_SSID].value || newwifi_c_secret != systemConfigList[SC_WIFI_C_SECRET].value)
		{
			DEBUG_SYSTEMCONFIG("Changing Control WIFI parameters\n");
			systemConfigList[SC_WIFI_C_ENABLE].value = newwifi_c_enable;
			systemConfigList[SC_WIFI_C_SSID].value = newwifi_c_ssid;
			systemConfigList[SC_WIFI_C_SECRET].value = newwifi_c_secret;
			writeConfig();
		}
		return true;
	}
	return false;
}

bool SystemConfig::validSsid(const String &ssid)
{
	return (ssid.length() < 1 && ssid.length() > 32) ? false : true;
}

bool SystemConfig::validSecret(const String &secret)
{
	return (secret.length() < 8) ? false : true;
}

bool SystemConfig::validChannel(const String &newChannel)
{
	if (newChannel == "")
		return true;
	long l = newChannel.toInt();
	return (l > 0 && l < 14) ? true : false;
}

bool SystemConfig::validInputsRate(const String &newAdc_rate)
{
	long l = newAdc_rate.toInt();
	if (l == 1 || l == 5 || l == 10 || l == 50 || l == 100)
		return true;
	else
		return false;
}

bool SystemConfig::validGpsMode(const String &new_gps_mode)
{
	if (new_gps_mode.equals("") || new_gps_mode.equals("nmea") || new_gps_mode.equals("ublox"))
		return true;
	else
		return false;
}

bool SystemConfig::validGpsRate(const String &newgps_rate)
{
	long l = newgps_rate.toInt();
	if (l == 1 || l == 5 || l == 10)
		return true;
	else
		return false;
}

bool SystemConfig::validEnable(const String &newenable)
{
	if (newenable.equals("0") || newenable.equals("1"))
		return true;
	else
		return false;
}

bool SystemConfig::validServerMode(const String &new_server_mode)
{
	if (new_server_mode.equals("") || new_server_mode.equals("mqtt") || new_server_mode.equals("mqtttls") || new_server_mode.equals("mqtttlsca"))
		return true;
	else
		return false;
}

bool SystemConfig::writeConfig()
{
#if SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_FS
	File f = SPIFFS.open(SYSTEM_CONFIG_FILENAME, "w");
	if (!f)
	{
		DEBUG_SYSTEMCONFIG("ERROR: Writing config\n");
		return false;
	}
	else
	{
		for (uint8_t i = 0; i < SYSTEMCONFIGLIST_SIZE; i++)
		{
			if (i >= 1)
				f.write('&');
			f.write((const uint8_t *)systemConfigList[i].key, systemConfigList[i].keylen);
			f.write('=');
			f.write((const uint8_t *)systemConfigList[i].value.c_str(), systemConfigList[i].value.length());
		}

		f.close();
		return true;
	}
#elif SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_PREFERENCES
	for (uint8_t i = 0; i < SYSTEMCONFIGLIST_SIZE; i++)
	{
		prefs.putString(systemConfigList[i].key, systemConfigList[i].value.c_str());
	}
	return true;
#endif
}

#if SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_FS
bool SystemConfig::loadConfig()
{
	String configLine;
	int attr, attrend, value, valueend, end;
	DEBUG_SYSTEMCONFIG("Reading configuration\n");

	File f = SPIFFS.open(SYSTEM_CONFIG_FILENAME, "r");
	if (!f)
	{
		DEBUG_SYSTEMCONFIG("ERROR: Opening config file\n");
		return false;
	}
	else
	{
		do
		{
			configLine += f.readString();
		} while (f.available());
		DEBUG_SYSTEMCONFIG("Config Line: %s\n", configLine.c_str());
		attr = 0;
		end = 0;
		do
		{
			attrend = configLine.indexOf('=', attr);
			configLine.setCharAt(attrend, 0);
			value = attrend + 1;
			valueend = configLine.indexOf('&', value);
			configLine.setCharAt(valueend, 0);

			/* till the end if , wasn't found */
			if (valueend == -1)
			{
				valueend = configLine.length();
				end = 1;
			}

			for (uint8_t u = 0; u < SYSTEMCONFIGLIST_SIZE; u++)
			{
				if (configLine.substring(attr, attrend).equals(systemConfigList[u].key))
				{
					systemConfigList[u].value = configLine.substring(value, valueend);
				}
			}

			attr = valueend + 1;
		} while (!end);
		f.close();
		return true;
	}
}

#elif SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_PREFERENCES

bool SystemConfig::loadConfig()
{
	for (uint8_t u = 0; u < SYSTEMCONFIGLIST_SIZE; u++)
	{
		systemConfigList[u].value = prefs.getString(systemConfigList[u].key);
	}
	return true;
}

#endif

String SystemConfig::getConfig()
{
	String configLine;
#if SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_FS
	if (SPIFFS.exists(SYSTEM_CONFIG_FILENAME))
	{
		File file = SPIFFS.open(SYSTEM_CONFIG_FILENAME, "r");
		do
		{
			configLine += file.readString();
		} while (file.available());
		file.close();
	}

#elif SYSTEM_CONFIG_STORAGE == SYSTEM_CONFIG_STORAGE_PREFERENCES
	uint8_t u;
	for (u = 0; u < (SYSTEMCONFIGLIST_SIZE - 1); u++)
	{
		configLine += systemConfigList[u].key;
		configLine += "=";
		configLine += prefs.getString(systemConfigList[u].key);
		configLine += "&";
	}
	configLine += systemConfigList[u].key;
	configLine += "=";
	configLine += prefs.getString(systemConfigList[u].key);

#endif
	return configLine;
}