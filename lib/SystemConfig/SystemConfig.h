
#ifndef _SYSTEMCONFIG_H_
#define _SYSTEMCONFIG_H_

#include <Arduino.h>

/* Supported storages */
#define SYSTEM_CONFIG_STORAGE_FS          1
#define SYSTEM_CONFIG_STORAGE_PREFERENCES 2

/* USE Filesystem for configurations storage */
// #define SYSTEM_CONFIG_STORAGE             SYSTEM_CONFIG_STORAGE_FS
// #define SYSTEM_CONFIG_FILENAME "/config"

/* OR  USE Preference library */
#define SYSTEM_CONFIG_STORAGE             SYSTEM_CONFIG_STORAGE_PREFERENCES
#define SYSTEM_CONFIG_NAME                "smmr"


#define SC_WIFI_SSID (uint8_t)0
#define SC_WIFI_SECRET (uint8_t)1
#define SC_WIFI_CHANNEL (uint8_t)2
#define SC_WIFI_HIDDEN (uint8_t)3
#define SC_WIFI_C_ENABLE (uint8_t)4
#define SC_WIFI_C_SSID (uint8_t)5
#define SC_WIFI_C_SECRET (uint8_t)6
#define SC_MEM_ENABLE (uint8_t)7
#define SC_GPS_MODE (uint8_t)8
#define SC_GPS_RATE (uint8_t)9
#define SC_SENSOR_CO2 (uint8_t)10
#define SC_A1_ENABLE (uint8_t)11
#define SC_A2_ENABLE (uint8_t)12
#define SC_D1_ENABLE (uint8_t)13
#define SC_D2_ENABLE (uint8_t)14
#define SC_D3_ENABLE (uint8_t)15
#define SC_D4_ENABLE (uint8_t)16
#define SC_INPUTS_RATE (uint8_t)17
#define SC_SERVER_MODE (uint8_t)18
#define SC_SERVER_HOST (uint8_t)19
#define SC_SERVER_TOPIC (uint8_t)20
#define SC_SERVER_USER (uint8_t)21
#define SC_SERVER_PASSWORD (uint8_t)22
#define SC_SERVER_COMMANDS (uint8_t)23
#define SC_UPDATE_FS_VER (uint8_t)24
#define SC_UPDATE_FW_VER (uint8_t)25
#define SC_UPDATE_FS (uint8_t)26
#define SC_UPDATE_FW (uint8_t)27

#define SYSTEMCONFIGLIST_SIZE (uint8_t)28

// typedef struct configItem {
//   const char key[];
//   String value;
// } configItem;

// configItem configList[] = {
//   ( "wifi_ssid", "SMMR" ),
//   ( "wifi_secret", "SMMR00000" )

// };

const char *const configKeys[] PROGMEM = {
    "wifi_ssid",
    "wifi_secret",
    "wifi_channel",
    "wifi_hidden",
    "wifi_c_enable",
    "wifi_c_ssid",
    "wifi_c_secret",
    "mem_enable",
    "gps_mode",
    "gps_rate",
    "sensor_co2",
    "a1_enable",
    "a2_enable",
    "d1_enable",
    "d2_enable",
    "d3_enable",
    "d4_enable",
    "inputs_rate",
    "server_mode",
    "server_host",
    "server_topic",
    "server_user",
    "server_password",
    "server_commands",
    "update_fs_ver",
    "update_fw_ver",
    "update_fs",
    "update_fw"
    };

const char C_WIFI_SSID[] PROGMEM = "wifi_ssid";
const char C_WIFI_SECRET[] PROGMEM = "wifi_secret";
const char C_WIFI_CHANNEL[] PROGMEM = "wifi_channel";
const char C_WIFI_HIDDEN[] PROGMEM = "wifi_hidden";
const char C_WIFI_C_ENABLE[] PROGMEM = "wifi_c_enable";
const char C_WIFI_C_SSID[] PROGMEM = "wifi_c_ssid";
const char C_WIFI_C_SECRET[] PROGMEM = "wifi_c_secret";

const char C_MEM_ENABLE[] PROGMEM = "mem_enable";

const char C_GPS_MODE[] PROGMEM = "gps_mode";
const char C_GPS_RATE[] PROGMEM = "gps_rate";

const char C_SENSOR_CO2[] PROGMEM = "sensor_co2";

const char C_A1_ENABLE[] PROGMEM = "a1_enable";
const char C_A2_ENABLE[] PROGMEM = "a2_enable";
const char C_D1_ENABLE[] PROGMEM = "d1_enable";
const char C_D2_ENABLE[] PROGMEM = "d2_enable";
const char C_D3_ENABLE[] PROGMEM = "d3_enable";
const char C_D4_ENABLE[] PROGMEM = "d4_enable";
const char C_INPUTS_RATE[] PROGMEM = "inputs_rate";

const char C_SERVER_MODE[] PROGMEM = "server_mode";
const char C_SERVER_HOST[] PROGMEM = "server_host";
const char C_SERVER_TOPIC[] PROGMEM = "server_topic";
const char C_SERVER_USER[] PROGMEM = "server_user";
const char C_SERVER_PASSWORD[] PROGMEM = "server_password";
const char C_SERVER_COMMANDS[] PROGMEM = "server_commands";

const char C_UPDATE_FS_VER[] PROGMEM = "update_fs_ver";
const char C_UPDATE_FW_VER[] PROGMEM = "update_fw_ver";
const char C_UPDATE_FS[] PROGMEM = "update_fs";
const char C_UPDATE_FW[] PROGMEM = "update_fw";

class SystemConfigItem
{
public:
  const char *key;
  uint8_t keylen;
  String value;
};

class SystemConfig
{
public:
  SystemConfig();
  bool begin();
  void end();
  bool resetConfig();
  bool writeConfig();

  String getConfig();
  
  const char *getValue(uint8_t key);
  const char *getValue(const char *key);
  const char *getValue(const String &key);

  String getValueStr(uint8_t key);
  String getValueStr(const char *key);
  String getValueStr(const String &key);

  bool setValue(uint8_t key, String &new_value);
  bool setValue(uint8_t key, const char *value);
  bool setValue(const String &key, const String &new_value);
  bool setValue(const String &key, const char *new_value);
  
  String system_id;

  bool setWifi(const String &wifi_ssid, const String &wifi_secret, const String &wifi_channel, const String &wifi_hidden);
  bool setWifiSsid(const String &wifi_ssid);
  bool setWifiSecret(const String &wifi_secret);
  bool setWifiChannel(const String &wifi_channel);
  bool setWifiHidden(const String &wifi_hidden);

  bool setControlWifi(const String &newcontrol_enable, const String &newcontrol_ssid, const String &newcontrol_secret);
    
private:
  SystemConfigItem systemConfigList[SYSTEMCONFIGLIST_SIZE];

  void loadDefaults();
  bool loadConfig();
  
  bool validKeyValue(uint8_t key, const String &value);

  bool validSsid(const String &wifi_ssid);
  bool validSecret(const String &wifi_secret);
  bool validChannel(const String &wifi_channel);
  
  bool validGpsMode(const String &new_gps_mode);
  bool validGpsRate(const String &gps_rate);
  bool validInputsRate(const String &ac_rate);
  bool validEnable(const String &enable);
  bool validServerMode(const String &new_server_mode);
};

extern SystemConfig config;

#endif
