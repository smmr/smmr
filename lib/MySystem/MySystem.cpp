/******************************************************************************

  Sebastián Guarino
  https://gitlab.com/sguarin/smmr.git

  Distributed as-is; no warranty is given.
******************************************************************************/

#include "MySystem.h"
#include <MyWebserver.h>
#include <App.h>
#include <HTTPClient.h>
#include <HTTPUpdate.h>

/*==================[macros and definitions]=================================*/

/* Disconnect WIFI if not connected after interval in seconds  */
#define MYSYSTEM_WIFI_TIMEOUT 10
/* Try to reconnect WIFI after interval in seconds */
#define MYSYSTEM_WIFI_RECONNECT 60
/* GMT offset of the RTC clock */
#define RTC_GMT_OFFSET 0
/* Daylight saving time offset */
#define RTC_DAYLIGHT_SAVING_TIME_OFFET 0
/* NTP server */
#define NTP_HOST "pool.ntp.org"

#if DEBUG
#ifndef DEBUG_ESP_PORT
#define DEBUG_ESP_PORT Serial
#endif
#ifndef DEBUG_ESP_WIFI
#define DEBUG_ESP_WIFI 1
#endif
#define DEBUG_MYSYSTEM(...)                   \
	DEBUG_ESP_PORT.print("DEBUG MySystem: "); \
	DEBUG_ESP_PORT.printf(__VA_ARGS__)
#else
#define DEBUG_MYSYSTEM(...)
#endif

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_MYSYSTEM(...)                  \
	INFO_ESP_PORT.print("INFO MySystem: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

/*==================[internal data definition]===============================*/

enum class wifiStateType
{
	UNCONFIGURED,
	CONFIGURED,
	CONFIGURED2,
	TO_CONNECT,
	WAITING_CONNECTION,
	WAITING,
	CONNECTED
};
wifiStateType wifiState;
bool wifiConfigure;

enum class systemStateType
{
	STARTUP,
	UPDATE,
	INIT,
	RUN
};
systemStateType systemState;
bool appConfigure;

bool updateConfigure;

MySystem mySystem;

/*==================[internal functions declaration]=========================*/

void updateWifiFSM();
void updateAppFSM();

void updateSystemFS();
void updateSystemFW();

/*==================[internal functions definition]==========================*/

void configureAp()
{
	bool ret;

	DEBUG_MYSYSTEM("Configuring AP\n");
	/* configure hostname */
#ifndef ESP32
	WiFi.hostname(config.getValueStr(SC_WIFI_SSID).c_str());
#else
	WiFi.setHostname(config.getValueStr(SC_WIFI_SSID).c_str());
#endif

	//WiFi.softAPdisconnect();
	if (config.getValueStr(SC_WIFI_CHANNEL).equals("") || config.getValueStr(SC_WIFI_C_ENABLE).equals("1"))
	{
		DEBUG_MYSYSTEM("Starting AP %s\n", config.getValue(SC_WIFI_SSID));
		ret = WiFi.softAP(config.getValue(SC_WIFI_SSID), config.getValue(SC_WIFI_SECRET));
		DEBUG_MYSYSTEM("WiFi.softAP returned %d\n", ret);
	}
	else
	{
		DEBUG_MYSYSTEM("Starting AP %s in channel %ld\n", config.getValue(SC_WIFI_SSID), config.getValueStr(SC_WIFI_CHANNEL).toInt());
		ret = WiFi.softAP(config.getValue(SC_WIFI_SSID), config.getValue(SC_WIFI_SECRET), config.getValueStr(SC_WIFI_CHANNEL).toInt(), config.getValueStr(SC_WIFI_HIDDEN).toInt());
		DEBUG_MYSYSTEM("WiFi.softAP returned %d\n", ret);
	}
}

void updateAppFSM()
{
	if (systemState == systemStateType::STARTUP)
	{
		// String current_version = config.getValueStr(SC_UPDATE_FW_VERSION);
		// if (!current_version.equals("") && !current_version.equals(GC_SYSTEM_VERSION_VALUE))
		// {
		// 	DEBUG_MYSYSTEM("changing systemState to UPDATE\n");
		// 	systemState = systemStateType::UPDATE;
		// }
		// else
		{
			DEBUG_MYSYSTEM("changing systemState to INIT\n");
			systemState = systemStateType::INIT;
		}
	}

	if (systemState == systemStateType::UPDATE)
	{
		DEBUG_MYSYSTEM("Ending App\n");
		app.end();

		updateSystemFS();
		updateSystemFW();

		DEBUG_MYSYSTEM("changing systemState to INIT\n");
		systemState = systemStateType::INIT;
	}

	if (systemState == systemStateType::INIT)
	{
		/* App initialization */
		DEBUG_MYSYSTEM("Ending App\n");
		app.end();

		DEBUG_MYSYSTEM("Reconfiguring App\n");
		app.setMemEnable(config.getValueStr(SC_MEM_ENABLE).equals("1"));
		app.setSensorCo2(config.getValueStr(SC_SENSOR_CO2).equals("1"));
		app.setGpsEnable(!config.getValueStr(SC_GPS_MODE).equals(""));

		if (config.getValueStr(SC_SERVER_MODE).equals("mqtt"))
		{
			app.setServerMode(MyMQTTMode::MODE_MQTT);
		}
		else if (config.getValueStr(SC_SERVER_MODE).equals("mqtttls"))
		{
			app.setServerMode(MyMQTTMode::MODE_MQTTTLS);
		}
		else if (config.getValueStr(SC_SERVER_MODE).equals("mqtttlsca"))
		{
			app.setServerMode(MyMQTTMode::MODE_MQTTTLS_CA);
		}
		else
		{
			app.setServerMode(MyMQTTMode::MODE_DISABLED);
		}
		app.setHost(config.getValueStr(SC_WIFI_SSID));
		app.setServerHost(config.getValueStr(SC_SERVER_HOST));
		app.setServerTopic(config.getValueStr(SC_SERVER_TOPIC));
		app.setServerUser(config.getValueStr(SC_SERVER_USER));
		app.setServerPassword(config.getValueStr(SC_SERVER_PASSWORD));
		app.setServerCommands(config.getValueStr(SC_SERVER_COMMANDS).equals("1"));

		app.setA1Enable(config.getValueStr(SC_A1_ENABLE).equals("1"));
		app.setA2Enable(config.getValueStr(SC_A2_ENABLE).equals("1"));
		app.setD1Enable(config.getValueStr(SC_D1_ENABLE).equals("1"));
		app.setD2Enable(config.getValueStr(SC_D2_ENABLE).equals("1"));
		app.setD3Enable(config.getValueStr(SC_D3_ENABLE).equals("1"));
		app.setD4Enable(config.getValueStr(SC_D4_ENABLE).equals("1"));
		app.setInputsRate(config.getValueStr(SC_INPUTS_RATE).toInt());

		DEBUG_MYSYSTEM("Starting App initialization\n");
		app.init();

		DEBUG_MYSYSTEM("changing systemState to RUN\n");
		systemState = systemStateType::RUN;
	}

	if (systemState == systemStateType::RUN)
	{
		if (appConfigure)
		{
			appConfigure = false;
			DEBUG_MYSYSTEM("changing systemState to INIT\n");
			systemState = systemStateType::INIT;
		}
		if (updateConfigure)
		{
			updateConfigure = false;
			DEBUG_MYSYSTEM("changing systemState to UPDATE\n");
			systemState = systemStateType::UPDATE;
		}
	}
}

void updateWifiFSM()
{
	static uint8_t count = 0;

	if (wifiState == wifiStateType::UNCONFIGURED)
	{
		/* Once Actions */
		WiFi.mode(WIFI_AP_STA);
		WiFi.setAutoConnect(false);
		WiFi.setAutoReconnect(false);
		wifiConfigure = false;

		/* Transitions */
		DEBUG_MYSYSTEM("changing wifiState to CONFIGURED\n");
		wifiState = wifiStateType::CONFIGURED;
		configureAp();
	}
	if (wifiState == wifiStateType::CONFIGURED)
	{

		/* Transitions */
		//wait for SYSTEM_EVENT_AP_START
		//WiFi.softAPConfig(...)

		//		WiFi.getStatusBits(AP_STARTED_BIT);
		DEBUG_MYSYSTEM("changing wifiState to CONFIGURED2\n");
		wifiState = wifiStateType::CONFIGURED2;
	}

	if (wifiState == wifiStateType::CONFIGURED2)
	{
		/* Transitions */
		if (config.getValueStr(SC_WIFI_C_ENABLE).equals("0") && wifiConfigure)
		{
			DEBUG_MYSYSTEM("changing wifiState to UNCONFIGURED\n");
			wifiState = wifiStateType::UNCONFIGURED;
			wifiConfigure = false;
		}
		else if (config.getValueStr(SC_WIFI_C_ENABLE).equals("1"))
		{
			DEBUG_MYSYSTEM("changing wifiState to TO_CONNECT\n");
			wifiState = wifiStateType::TO_CONNECT;
			wifiConfigure = false;
		}
	}

	if (wifiState == wifiStateType::TO_CONNECT)
	{
		/* Once Actions */
		DEBUG_MYSYSTEM("Connecting to external network\n");
		WiFi.config(IPAddress(0, 0, 0, 0), IPAddress(0, 0, 0, 0), IPAddress(0, 0, 0, 0));
		WiFi.begin(config.getValue(SC_WIFI_C_SSID), config.getValue(SC_WIFI_C_SECRET));
		count = 0;

		/* Transitions */
		DEBUG_MYSYSTEM("changing wifiState to WAITING_CONNECTION\n");
		wifiState = wifiStateType::WAITING_CONNECTION;
	}

	if (wifiState == wifiStateType::WAITING_CONNECTION)
	{
		/* Actions */
		count++;

		/* Transitions */
		if (WiFi.isConnected())
		{
			DEBUG_MYSYSTEM("changing wifiState to CONNECTED\n");
			wifiState = wifiStateType::CONNECTED;
			configTime(RTC_GMT_OFFSET, RTC_DAYLIGHT_SAVING_TIME_OFFET, NTP_HOST);
		}
		else if (!WiFi.isConnected() && count > MYSYSTEM_WIFI_TIMEOUT)
		{
			DEBUG_MYSYSTEM("changing wifiState to WAITING\n");
			WiFi.disconnect();
			wifiState = wifiStateType::WAITING;
		}
	}

	if (wifiState == wifiStateType::WAITING)
	{
		/* Actions */
		count++;

		/* Transitions */
		if (!config.getValueStr(SC_WIFI_C_ENABLE).equals("1") || wifiConfigure)
		{
			WiFi.disconnect();
			wifiConfigure = false;
			DEBUG_MYSYSTEM("changing wifiState to UNCONFIGURED\n");
			wifiState = wifiStateType::UNCONFIGURED;
		}
		else if (count > MYSYSTEM_WIFI_RECONNECT)
		{
			DEBUG_MYSYSTEM("changing wifiState to TO_CONNECT\n");
			wifiState = wifiStateType::TO_CONNECT;
		}
	}

	if (wifiState == wifiStateType::CONNECTED)
	{
		/* Actions */

		/* Transitions */
		if (wifiConfigure)
		{
			WiFi.disconnect();
			wifiConfigure = false;
			DEBUG_MYSYSTEM("changing wifiState to UNCONFIGURED\n");
			wifiState = wifiStateType::UNCONFIGURED;
		}
		else if (!WiFi.isConnected())
		{
			DEBUG_MYSYSTEM("changing wifiState to TO_CONNECT\n");
			wifiState = wifiStateType::TO_CONNECT;
		}
	}
}

void updateSystemFS()
{
	String next_version = config.getValueStr(SC_UPDATE_FS_VER);
	if (!next_version.equals("") && !next_version.equals(status.getFSVersion()))
	{

		WiFiClientSecure client;
		client.setInsecure();
		INFO_MYSYSTEM("Updating filesystem to %s\n", next_version.c_str());
		HTTPUpdateResult ret = httpUpdate.updateSpiffs(client, config.getValueStr(SC_UPDATE_FS));
		switch (ret)
		{
		case HTTP_UPDATE_FAILED:
			INFO_MYSYSTEM("HTTP_UPDATE_FAILED Error (%d): %s\n", httpUpdate.getLastError(), httpUpdate.getLastErrorString().c_str());
			break;

		case HTTP_UPDATE_NO_UPDATES:
			INFO_MYSYSTEM("HTTP_UPDATE_NO_UPDATES");
			break;

		case HTTP_UPDATE_OK:
			INFO_MYSYSTEM("HTTP_UPDATE_OK");
			break;
			
		default:
			INFO_MYSYSTEM("HTTP_UPDATE error: %u", ret);
			break;
		}
	}
}

void updateSystemFW()
{
	String next_version = config.getValueStr(SC_UPDATE_FW_VER);
	if (!next_version.equals("") && !next_version.equals(GC_SYSTEM_VERSION_VALUE))
	{
		WiFiClientSecure client;
		client.setInsecure();
		INFO_MYSYSTEM("Updating firmware to %s\n", next_version.c_str());

		//httpUpdate.setLedPin(LED_BUILTIN, HIGH);
		HTTPUpdateResult ret = httpUpdate.update(client, config.getValueStr(SC_UPDATE_FW));
		switch (ret)
		{
		case HTTP_UPDATE_FAILED:
			INFO_MYSYSTEM("HTTP_UPDATE_FAILED Error (%d): %s\n", httpUpdate.getLastError(), httpUpdate.getLastErrorString().c_str());
			break;

		case HTTP_UPDATE_NO_UPDATES:
			INFO_MYSYSTEM("HTTP_UPDATE_NO_UPDATES");
			break;

		case HTTP_UPDATE_OK:
			INFO_MYSYSTEM("HTTP_UPDATE_OK");
			break;
		default:
			INFO_MYSYSTEM("HTTP_UPDATE unknown error: %u", ret);
		}
	}
}

void mySystemTask(void *taskParmPtr)
{
	DEBUG_MYSYSTEM("Starting Task mySystemTask\n");
	for (;;)
	{
		DEBUG_MYSYSTEM("running mySystemTask\n");
		updateWifiFSM();
		updateAppFSM();

		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}

/* MySystem Class Implementation */
void MySystem::init()
{
	/* initialize object members with defaults */
	config.begin();
	status.init();

	wifiState = wifiStateType::UNCONFIGURED;
	systemState = systemStateType::STARTUP;

	/* true means wifi must be reconfigured */
	wifiConfigure = false;

	xTaskCreatePinnedToCore(
		mySystemTask,	/* Function to implement the task */
		"mySystemTask", /* Name of the task */
		10000,			/* Stack size in words */
		NULL,			/* Task input parameter */
		2,				/* Priority of the task */
		NULL,			/* Task handle. */
		1);				/* Core where the task should run */

#warning delay here
	delay(5000);

	/* Webserver initialization */
	DEBUG_MYSYSTEM("Starting MyWebserver initialization\n");
	webserver.init(getConfig, setConfig, getStatus);
	webserver.run();
}

bool MySystem::setConfig(const String &parameter, const String &value)
{
	if (config.setValue(parameter, value))
	{
		if (parameter.equals("wifi_ssid"))
		{
			wifiConfigure = true;
			appConfigure = true;
		}
		else if (parameter.startsWith("wifi_"))
		{
			wifiConfigure = true;
		}
		else if (parameter.startsWith("update_"))
		{
			updateConfigure = true;
		}
		else
		{
			appConfigure = true;
		}
		return true;
	}
	else
	{
		return false;
	}
}

String MySystem::getStatus()
{
	String statusStr = status.getStatus();
	statusStr.concat("&");
	statusStr.concat(app.getStatus());
	return statusStr;
}

String MySystem::getConfig()
{
	return config.getConfig();
}