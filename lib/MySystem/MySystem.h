
#ifndef MYSYSTEM_H_
#define MYSYSTEM_H_

#include <Arduino.h>
#include <SystemStatus.h>
#include <SystemConfig.h>
#ifdef ESP32
#include <WiFi.h>
#else
#include <ESP8266WiFi.h>
#endif

class MySystem
{
  public:
    void init();

  private:
    /* Functions */
    static bool setConfig(const String &parameter, const String &value);

    //void systemTask(void * pvParameters);
    //static void wifiTask(void* taskParmPtr);

    static String getStatus();
    static String getConfig();

};

extern MySystem mySystem;

#endif // MYSYSTEM_H_
