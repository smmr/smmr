/******************************************************************************

  Sebastián Guarino

  Distributed as-is; no warranty is given.
******************************************************************************/

#ifndef _MYWEBSERVER_H_
#define _MYWEBSERVER_H_

#include <Arduino.h>
#ifdef ESP32
#include <WebServer.h>
#else
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266WiFi.h>
#endif
#include <FS.h>
#ifdef ESP32
#include <SPIFFS.h>
#endif

/* ROM Stored strings */
const char GC_TEXT_PLAIN[] PROGMEM = "text/plain";
const char GC_TEXT_HTML[] PROGMEM = "text/html";

typedef std::function<String()> THandlerGetConfigFunction;
typedef std::function<bool(const String& config, const String& value)> THandlerSetConfigFunction;
typedef std::function<String()> THandlerGetStatusFunction;

class MyWebserver
{
  public:
    bool init(THandlerGetConfigFunction pGetConfigCallback, THandlerSetConfigFunction pSetConfigCallback, THandlerGetStatusFunction pGetStatusCallback);
    void loop();
    void run();

  protected:
    static void task(void *pvParameters);

  private:
    static void sendNoCacheHeaders();
    static String getContentType(String filename);

    static void handle404();
    static bool handleFileRead(String path, bool cache = true);
    static void handleIndex();
    static void handleStatus();
    static void handleConfig();
    static void handleSetConfig();
    static void handleData();
    static void handleDownload();
};

extern MyWebserver webserver;

#endif
