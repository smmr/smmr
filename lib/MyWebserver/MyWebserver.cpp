/******************************************************************************

  Sebastián Guarino

  Distributed as-is; no warranty is given.
******************************************************************************/

#include <MyWebserver.h>
#include <SystemConfig.h>
#include <MySD.h>
#include <ArduinoOTA.h>
#include <MyMQTT.h>

/*==================[macros and definitions]=================================*/

#if DEBUG
#ifndef DEBUG_ESP_PORT
#define DEBUG_ESP_PORT Serial
#endif
#ifndef DEBUG_ESP_WIFI
#define DEBUG_ESP_WIFI 1
#endif
#define DEBUG_MYWEBSERVER(...)                   \
	DEBUG_ESP_PORT.print("DEBUG MyWebserver: "); \
	DEBUG_ESP_PORT.printf(__VA_ARGS__)
#else
#define DEBUG_MYWEBSERVER(...)
#endif

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_MYWEBSERVER(...)                  \
	INFO_ESP_PORT.print("INFO MyWebserver: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)


/*==================[internal data definition]===============================*/

#ifdef ESP32
WebServer server(80);
#else
ESP8266WebServer server(80);
#endif

THandlerGetConfigFunction pGetConfig;
THandlerSetConfigFunction pSetConfig;
THandlerGetStatusFunction pGetStatus;

MyWebserver webserver;

/*==================[internal functions declaration]=========================*/

/*==================[internal functions definition]==========================*/

void MyWebserver::task(void *pvParameters)
{
	MyWebserver *p = static_cast<MyWebserver *>(pvParameters);
	DEBUG_MYWEBSERVER("Starting Task MyWebserverTask\n");
	for (;;)
	{
		p->loop();
		vTaskDelay(10 / portTICK_PERIOD_MS);
	}
}

/* Constructor */
bool MyWebserver::init(THandlerGetConfigFunction pGetConfigCallback, THandlerSetConfigFunction pSetConfigCallback, THandlerGetStatusFunction pGetStatusCallback)
{
	/* initialize object members with defaults */
	bool ret = true;

	pGetConfig = pGetConfigCallback;
	pSetConfig = pSetConfigCallback;
	pGetStatus = pGetStatusCallback;

	/* FS start */
	if (!SPIFFS.begin())
	{
		INFO_MYWEBSERVER("ERROR: could not mount SPIFFS\n");
	}

	/* Configure Webserver */
	server.on("/", handleIndex);
	server.on("/u/setconfig", handleSetConfig);
	server.on("/u/status", handleStatus);
	server.on("/u/config", handleConfig);
	server.on("/u/data", handleData);
	server.on("/u/dow", handleDownload);

	//	server.on("/a", [](){
	//		DEBUG_MAIN("INFO: Serving /a/index.html\n");
	//		if (!server.authenticate(ADMIN_USERNAME, ADMIN_PASSWORD))
	//			return server.requestAuthentication();
	//		if (!handleFileRead("/h/a/index.html"))
	//			handle404();
	//	});
	//	server.on("/a/setconfig", handleAdminSetConfig);
	//	server.on("/a/status", handleAdminStatus);

	server.onNotFound([]() {
		String url = String("/h");
		url.concat(server.uri());
		DEBUG_MYWEBSERVER("INFO: Serving %s\n", url.c_str());
		if (!handleFileRead(url))
			handle404();
	});

	// OTA Optionals
  	// Port defaults to 3232
  	// ArduinoOTA.setPort(3232);

  	// Hostname defaults to esp3232-[MAC]
  	// ArduinoOTA.setHostname("myesp32");

  	// No authentication by default
  	// ArduinoOTA.setPassword("admin");

  	// Password can be set with it's md5 value as well
  	// MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  	// ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

	ArduinoOTA.onStart([]() {
		String type;
		if (ArduinoOTA.getCommand() == U_FLASH) {
			type = "sketch";
			DEBUG_MYWEBSERVER("Start updating firmware\n");
		} else {
			// U_SPIFFS
			type = "filesystem";
			DEBUG_MYWEBSERVER("Start updating filesystem\n");
			// NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
			#warning FIXME
		}
	});
	ArduinoOTA.onEnd([]() {
		DEBUG_MYWEBSERVER("End\n");
	});
	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
		Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
	});
	ArduinoOTA.onError([](ota_error_t error) {
		DEBUG_MYWEBSERVER("OTA Error[%u]: ", error);
		if (error == OTA_AUTH_ERROR)
		{
			DEBUG_MYWEBSERVER("Auth Failed\n");
		}
		else if (error == OTA_BEGIN_ERROR)
		{
			DEBUG_MYWEBSERVER("Begin Failed\n");
		}
		else if (error == OTA_CONNECT_ERROR)
		{
			DEBUG_MYWEBSERVER("Connect Failed\n");
		}
		else if (error == OTA_RECEIVE_ERROR)
		{
			DEBUG_MYWEBSERVER("Receive Failed\n");
		}
		else if (error == OTA_END_ERROR)
		{
			DEBUG_MYWEBSERVER("End Failed\n");
		}
	});

	// Start servers
	server.begin();
	ArduinoOTA.begin();
	mqtt.setCommandsHooks(pGetConfig, pSetConfig, pGetStatus);

	return ret;
}

void MyWebserver::run()
{
	xTaskCreatePinnedToCore(
		task,			   /* Function to implement the task */
		"myWebserverTask", /* Name of the task */
		10000,			   /* Stack size in words */
		this,			   /* Task input parameter */
		3,				   /* Priority of the task */
		NULL,			   /* Task handle. */
		1);				   /* Core where the task should run */
}

void MyWebserver::loop()
{
	server.handleClient();
	ArduinoOTA.handle();
	mqtt.loop();
}

bool askAuthFromOutside(void)
{
	// If local address of opened socket is the outside address obtenied via WiFi
	// we will require authentication
	if (server.client().localIP() == WiFi.localIP())
		if (!server.authenticate(config.getValue(SC_WIFI_SSID), config.getValue(SC_WIFI_SECRET)))
		{
			server.requestAuthentication();
			return true;
		}
	return false;
}

void MyWebserver::sendNoCacheHeaders()
{
	server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
	server.sendHeader("Pragma", "no-cache");
	server.sendHeader("Expires", "-1");
}

String MyWebserver::getContentType(String filename)
{
	if (filename.endsWith(".htm"))
		return "text/html";
	else if (filename.endsWith(".html"))
		return "text/html";
	else if (filename.endsWith(".css"))
		return "text/css";
	else if (filename.endsWith(".js"))
		return "application/javascript";
	else if (filename.endsWith(".png"))
		return "image/png";
	else if (filename.endsWith(".gif"))
		return "image/gif";
	else if (filename.endsWith(".jpg"))
		return "image/jpeg";
	else if (filename.endsWith(".png"))
		return "image/png";
	else if (filename.endsWith(".ico"))
		return "image/x-icon";
	else if (filename.endsWith(".gz"))
		return "application/x-gzip";
	return "text/plain";
}

void MyWebserver::handle404()
{
	sendNoCacheHeaders();
	server.send(404, "text/plain", "Pagina no encontrada");
}

bool MyWebserver::handleFileRead(String path, bool cache)
{
	if (askAuthFromOutside())
		return true;

	if (cache)
	{
		server.sendHeader("Cache-Control", "public, max-age=31536000");
		//server.sendHeader("Expires", "-1");
	}
	else
	{
		sendNoCacheHeaders();
	}

	if (path.endsWith("/"))
		path += "index.html";
	String contentType = getContentType(path);
	String pathWithGz = path + ".gz";
	if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path))
	{
		if (SPIFFS.exists(pathWithGz))
			path += ".gz";
		File file = SPIFFS.open(path, "r");
		server.streamFile(file, contentType);
		file.close();
		return true;
	}
	return false;
}

void MyWebserver::handleIndex()
{
	if (askAuthFromOutside())
		return;

	sendNoCacheHeaders();
	DEBUG_MYWEBSERVER("Server.client().localIP(): %s\n", server.client().localIP().toString().c_str());
	DEBUG_MYWEBSERVER("WiFi.localIP(): %s\n", WiFi.localIP().toString().c_str());

	DEBUG_MYWEBSERVER("INFO: Redirect to u/index.html\n");
	server.sendHeader("Location", "u/index.html");
	server.send(302, "text/plain", "");
}

void MyWebserver::handleStatus()
{
	if (askAuthFromOutside())
		return;

	sendNoCacheHeaders();

	server.send(200, FPSTR(GC_TEXT_HTML), pGetStatus());
}

void MyWebserver::handleConfig()
{
	if (askAuthFromOutside())
		return;

	sendNoCacheHeaders();

	server.send(200, FPSTR(GC_TEXT_HTML), pGetConfig());
}

void MyWebserver::handleSetConfig()
{
	bool ret = true;
	String error = "Error: configurando";

	if (askAuthFromOutside())
		return;

	sendNoCacheHeaders();

	for (uint8_t i = 0; i < server.args(); i++)
	{
		if (!pSetConfig(server.argName(i), server.arg(i)))
		{
			ret = false;
			error.concat(" ");
			error.concat(server.argName(i));
		}
	}

	server.send(200, FPSTR(GC_TEXT_HTML), (ret) ? "1" : error);
}

void MyWebserver::handleData()
{
	if (askAuthFromOutside())
		return;

	sendNoCacheHeaders();

	if (server.arg("del").length() > 0)
	{
		MYSD_SD.remove((server.arg("del")));
	}
	server.send(200, FPSTR(GC_TEXT_HTML), sd.getDir("/"));
}

void MyWebserver::handleDownload()
{
	String dataType = GC_TEXT_PLAIN;
	File dataFile;
	String fileName = server.arg("f");

	if (askAuthFromOutside())
		return;

	if (server.arg("f").length() > 0)
	{
		dataFile = MYSD_SD.open(fileName);

		if (!dataFile)
			return;

		String headerName = "Content-Disposition";
		String headerValue = "Attachment; filename=";
		headerValue.concat(fileName);
		server.sendHeader(headerName, headerValue, false);
		if (server.streamFile(dataFile, dataType) != dataFile.size())
		{
			INFO_MYWEBSERVER("Archivo %s: enviado menos de lo esperado\n", fileName.c_str());
		}

		dataFile.close();
	}
}
