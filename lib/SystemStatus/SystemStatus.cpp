/******************************************************************************

  Sebastián Guarino

  Distributed as-is; no warranty is given.
******************************************************************************/

#include "SystemStatus.h"
#ifdef ESP32
#include <WiFi.h>
#else
#include <ESP8266WiFi.h> /* WiFi is extern here */
#endif
#include <FS.h>
#ifdef ESP32
#include <SPIFFS.h>
#endif

/*==================[macros and definitions]=================================*/

#ifndef ESP32
/* necesary to use ESP.getVcc() */
ADC_MODE(ADC_VCC);
#endif
extern "C" int rom_phy_get_vdd33();

/*==================[internal data definition]===============================*/

SystemStatus status;

/*==================[internal functions declaration]=========================*/

/*==================[internal functions definition]==========================*/

/* Constructor */
void SystemStatus::init() {
	/* initialize object members with defaults */
	sd_status = "";
	sd_size = "";
	sd_free = "";

	bat_level = "";
}

uint8_t SystemStatus::getWifiClients() {
#ifdef ESP32
	uint8_t softap_stations_cnt = WiFi.softAPgetStationNum();
#else
	uint8_t softap_stations_cnt = wifi_softap_get_station_num();
#endif
	return (softap_stations_cnt < 0) ? 0 : softap_stations_cnt;
}

String SystemStatus::getStatus() {
	String line = "";

	line.concat(FPSTR(GC_BAT_LEVEL));
	line.concat("=");
	line.concat(getVCC());

	line.concat("&");
	line.concat(FPSTR(GC_WIFI_CLIENTS));
	line.concat("=");
	line.concat(getWifiClients());

	line.concat("&");
	line.concat(FPSTR(GC_WIFI_CHANNEL));
	line.concat("=");
	line.concat(WiFi.channel());

	line.concat("&");
	line.concat(FPSTR(GC_WIFI_RSSI));
	line.concat("=");
	line.concat(WiFi.RSSI());

	line.concat("&");
	line.concat(FPSTR(GC_WIFI_STATUS));
	line.concat("=");
	line.concat(wifiStatus());

	line.concat("&");
	line.concat(FPSTR(GC_WIFI_IP));
	line.concat("=");
	line.concat(WiFi.localIP().toString());

	line.concat("&");
	line.concat(FPSTR(GC_SYSTEM_FS_VERSION));
	line.concat("=");
	line.concat(getFSVersion());

	return line;
}

String SystemStatus::wifiStatus() {
	String r;
	wl_status_t wifiRet = WiFi.status();
	switch (wifiRet) {
		case WL_IDLE_STATUS:
			return "Esperando";
		case WL_CONNECTED:
			return "Conectado";
		case WL_CONNECT_FAILED:
			return "Clave incorrecta";
		case WL_CONNECTION_LOST:
			return "Error de conexión";
		case WL_NO_SSID_AVAIL:
			return "Red no disponible";
		case WL_DISCONNECTED:
			return "Desconectado";
		default:
			r = "Error ";
			r.concat(WiFi.status());
			return r;
	}	
}

String SystemStatus::getVCC() {
	String vcc = "";
#ifdef ESP32
	float voltage = ((float)rom_phy_get_vdd33()) / 1000; 
	vcc.concat(voltage);
#else
	vcc = ESP.getVcc();
#endif
	return vcc;
}

String SystemStatus::getFSVersion() {
	String version;
	File file = SPIFFS.open("/h/VERSION", "r");
	version = file.readString();
	file.close();
	return version;
}

String SystemStatus::getAdminStatus() {
	String line = "";

	line.concat("chip_sdk_version=");
	line.concat(ESP.getSdkVersion());

	line.concat("&chip_free_heap=");
	line.concat(ESP.getFreeHeap());

	line.concat("&chip_last_reset=");
	line.concat(ESP.getCycleCount());

	line.concat("&flash_speed=");
	line.concat(ESP.getFlashChipSpeed());

	line.concat("&chip_vcc=");
	line.concat(getVCC());

	line.concat("&chip_id=");
#ifdef ESP32
	uint64_t chipId = ESP.getEfuseMac();
	// high 4 bytes
	line.concat(String(uint32_t(chipId >> 32), HEX));
	// low 4 bytes
	line.concat(String(uint32_t(chipId), HEX));
#else
	line.concat(ESP.getChipId());

	line.concat("&chip_boot_version=");
	line.concat(ESP.getBootVersion());
#endif

	line.concat("&flash_sketch_size=");
	line.concat(ESP.getSketchSize());

	line.concat("&flash_free_sketch_space=");
	line.concat(ESP.getFreeSketchSpace());

#ifndef ESP32
	line.concat("&chip_boot_mode=");
	line.concat(ESP.getBootMode());

	line.concat("&chip_reset_info=");
	line.concat(ESP.getResetInfo());

	line.concat("&chip_reset_reason=");
	line.concat(ESP.getResetReason());

	line.concat("&flash_id=");
	line.concat(ESP.getFlashChipId());

	line.concat("&flash_size=");
	line.concat(ESP.getFlashChipRealSize());
#endif

	return line;
}
