
#ifndef _SYSTEMSTATUS_H_
#define _SYSTEMSTATUS_H_

#include <Arduino.h>

#ifdef DEBUG
  #ifndef DEBUG_ESP_PORT
    #define DEBUG_ESP_PORT Serial
  #endif
  #ifndef DEBUG_ESP_WIFI
    #define DEBUG_ESP_WIFI 1
  #endif
  #define DEBUG_SYSTEMSTATUS(...) DEBUG_ESP_PORT.print("DEBUG SystemStatus: "); DEBUG_ESP_PORT.printf( __VA_ARGS__ )
#else
  #define DEBUG_SYSTEMSTATUS(...)
#endif

/* ROM Stored Strings */
const char GC_BAT_LEVEL[] PROGMEM = "bat_level";

const char GC_WIFI_MODE[] PROGMEM = "wifi_mode";
const char GC_WIFI_CLIENTS[] PROGMEM = "wifi_clients";
const char GC_WIFI_CHANNEL[] PROGMEM = "wifi_channel";
const char GC_WIFI_RSSI[] PROGMEM = "wifi_rssi";
const char GC_WIFI_STATUS[] PROGMEM = "wifi_status";
const char GC_WIFI_IP[] PROGMEM = "wifi_ip";

const char GC_SYSTEM_FS_VERSION[] PROGMEM = "system_fs_version";

class SystemStatus
{
  public:
    void init();

    String sd_status;
    String sd_size;
    String sd_free;

    String bat_level;

    String data;

    String getStatus();
    String getAdminStatus();

    String getFSVersion();

  private:
    uint8_t getWifiClients();
    String getVCC();
    String wifiStatus();
};

extern SystemStatus status;

#endif
