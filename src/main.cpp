/******************************************************************************

  Sebastián Guarino
  https://gitlab.com/sguarin/smmr.git

  Distributed as-is; no warranty is given.
******************************************************************************/

/*==================[inclusions]=============================================*/
#include <Arduino.h>
#include <MySystem.h>
#include <main.h>

/*==================[macros and definitions]=================================*/

#define SERIAL_MUTEX_LOCK()    do {} while (xSemaphoreTake(serialLock, portMAX_DELAY) != pdPASS)
#define SERIAL_MUTEX_UNLOCK()  xSemaphoreGive(serialLock)

#if DEBUG
  #ifndef DEBUG_ESP_PORT
    #define DEBUG_ESP_PORT Serial
  #endif
  #ifndef DEBUG_ESP_WIFI
    #define DEBUG_ESP_WIFI 1
  #endif
  #define DEBUG_MAIN(...) SERIAL_MUTEX_LOCK(); DEBUG_ESP_PORT.print("DEBUG MAIN: "); DEBUG_ESP_PORT.printf( __VA_ARGS__ ); SERIAL_MUTEX_UNLOCK()
#else
  #define DEBUG_MAIN(...)
#endif

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_MAIN(...) SERIAL_MUTEX_LOCK(); INFO_ESP_PORT.print("INFO MAIN: "); INFO_ESP_PORT.printf( __VA_ARGS__ ) ; SERIAL_MUTEX_UNLOCK()

/*==================[internal data declaration]==============================*/

SemaphoreHandle_t serialLock;

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

#ifndef UNIT_TEST

//! Arduino setup run once at start of main
void setup()
{
	// Delay a few secs
	delay(STARTUP_DELAY);

	// Serial initialization
	Serial.begin(SERIAL_BAUD_RATE);

	// create serial MUTEX
	serialLock = xSemaphoreCreateMutex();
	if (serialLock == nullptr) {
		INFO_ESP_PORT.print("INFO MAIN: Error creating serial MUTEX\n");
		//INFO_MAIN("Error creating serial MUTEX\n");
	}

	/* MySystem Init */
	mySystem.init();

	INFO_MAIN("Setup initialization finished\n");
}

//! Arduino Loop hook. Whatchdog happens inside this task
void loop()
{
	uint32_t freeHeap = ESP.getFreeHeap();
	if (freeHeap < 50000) {
		INFO_MAIN("Freeheap %u: Restarting!\n", freeHeap);
		ESP.restart();
	}
	INFO_MAIN("Freeheap %u\n", freeHeap);
    delay(WATCHDOG_PERIOD); //Don't spam the CPU
}

#endif // UNIT_TEST

/*==================[end of file]============================================*/
