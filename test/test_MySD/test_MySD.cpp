#ifdef UNIT_TEST

#include <MySD.h>
#include <unity.h>

#ifndef TESTS_NUM
#define TESTS_NUM 2
#endif
#ifndef TESTS_DELAY
#define TESTS_DELAY 50
#endif

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_TEST(...)                  \
	INFO_ESP_PORT.print("INFO TEST: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

void test_create_new_file()
{
	sd.removeFile();
	TEST_ASSERT_TRUE(sd.appendLine("1"));
}

void test_remove()
{
	sd.appendLine("1");
	TEST_ASSERT_TRUE(sd.removeFile());
}

void test_remove_unexistent()
{
	sd.removeFile("/unexistent");
	TEST_ASSERT_FALSE(sd.removeFile("/unexistent"));
}

void test_hello_world()
{
	const char *TEST_STRING = "Hello World";
	String tmp;

	sd.removeFile();
	TEST_ASSERT_TRUE(sd.appendLine(TEST_STRING));
	TEST_ASSERT_TRUE(sd.readLine(tmp));
	TEST_ASSERT_EQUAL_STRING(TEST_STRING, tmp.c_str());
}

void test_hello_world2()
{
	const char *TEST_STRING = "Hello World\nSarasa\n";
	const char *TEST_STRING2 = "Hello World";
	String tmp;

	sd.removeFile();
	TEST_ASSERT_TRUE(sd.appendLine(TEST_STRING));
	TEST_ASSERT_TRUE(sd.readLine(tmp));
	TEST_ASSERT_EQUAL_STRING(TEST_STRING2, tmp.c_str());
}

void test_append_hello_world()
{
	const char *TEST_STRING = "Hello World\n";
	String tmp;

	TEST_ASSERT_TRUE(sd.appendLine(TEST_STRING));
}

void testFileIO()
{
}

void setup()
{
	// NOTE!!! Wait for >2 secs
	// if board doesn't support software reset via Serial.DTR/RTS
	delay(2000);

	UNITY_BEGIN(); // IMPORTANT LINE!

	sd.init();
}

uint8_t i = 0;
uint8_t j;

void loop()
{
	if (i < TESTS_NUM)
	{
		INFO_TEST("Se crea un archivo nuevo\n");
		RUN_TEST(test_create_new_file);

		INFO_TEST("Se borra un archivo existente\n");
		RUN_TEST(test_remove);

		INFO_TEST("Se borra un archivo inexistente\n");
		RUN_TEST(test_remove_unexistent);

		INFO_TEST("Se crea un archivo con la cadena \"hello world\"\n");
		RUN_TEST(test_hello_world);

		INFO_TEST("Se crea un archivo con una cadena cuya primer linea es \"hello world\"\n");
		RUN_TEST(test_hello_world2);

		j = 0;
		while (j < 100)
		{
			INFO_TEST("Se realiza un append de la cadena \"hello world\"\n");
			RUN_TEST(test_append_hello_world);
			j++;
		}
		delay(TESTS_DELAY);
		i++;
	}
	else
	{
		UNITY_END(); // stop unit testing
	}
}

#endif
