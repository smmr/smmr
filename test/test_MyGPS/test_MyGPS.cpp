#include <Arduino.h>
#include <MyGPS.h>
#include <unity.h>

#define TESTS_NUM 10
#define TESTS_DELAY 500

#if DEBUG
  #ifndef DEBUG_ESP_PORT
    #define DEBUG_ESP_PORT Serial
  #endif
  #define DEBUG_TEST(...) DEBUG_ESP_PORT.print("DEBUG TEST: "); DEBUG_ESP_PORT.printf( __VA_ARGS__ )
#else
  #define DEBUG_TEST(...)
#endif

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_TEST(...)                  \
	INFO_ESP_PORT.print("INFO TEST: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

void test_begin()
{
	TEST_ASSERT_TRUE(gps.begin());
}

void test_timeStr()
{
	String timeStr = gps.getTimeStr();
	INFO_TEST("getTimeStr(): %s\n", timeStr.c_str());
}

void test_getLat()
{
	// exact limits, as specified by EPSG:900913 / EPSG:3785 / OSGEO:41001
	// also check for zero
	double minLat = -85.05112878;
	double maxLat = 85.05112878;
	double lat = gps.getLat();
	DEBUG_TEST("getLat(): %f\n", lat);
	TEST_ASSERT(lat > minLat && lat < maxLat && lat != 0);
}

void test_getLng()
{
	// exact limits, as specified by EPSG:900913 / EPSG:3785 / OSGEO:41001
	// also check for zero
	double minLng = -180;
	double maxLng = 180;
	double lng = gps.getLng();
	DEBUG_TEST("getLng(): %f\n", lng);
	TEST_ASSERT(lng > minLng && lng < maxLng && lng != 0);
}

void test_getAlt()
{
	//
	double minAlt = -50;
	double maxAlt = 200;
	double alt = gps.getAlt();
	DEBUG_TEST("getAlt(): %f\n", alt);
	TEST_ASSERT(alt > minAlt && alt < maxAlt);
}

void test_getSatellites()
{
	//
	int minSats = 1;
	int maxSats = 10;
	int sats = gps.getSatellites();

	DEBUG_TEST("getSatellites(): %i\n", sats);
	TEST_ASSERT(sats >= minSats && sats <= maxSats);
}

void setup()
{
	// NOTE!!! Wait for >2 secs
	// if board doesn't support software reset via Serial.DTR/RTS
	delay(2000);

	UNITY_BEGIN(); // IMPORTANT LINE!

	INFO_TEST("Initializing GPS\n");
	RUN_TEST(test_begin);
	delay(2000);
}

uint8_t i = 0;

void loop()
{
	if (i < TESTS_NUM)
	{
		INFO_TEST("Checking getTimeStr()\n");
		RUN_TEST(test_timeStr);

		INFO_TEST("Checking getLat()\n");
		RUN_TEST(test_getLat);

		INFO_TEST("Checking getLng()\n");
		RUN_TEST(test_getLng);

		INFO_TEST("Checking getAlt()\n");
		RUN_TEST(test_getAlt);

		INFO_TEST("Checking getSatellites()\n");
		RUN_TEST(test_getSatellites);

		delay(TESTS_DELAY);
		i++;
	}
	else
	{
		UNITY_END(); // stop unit testing
	}
}

