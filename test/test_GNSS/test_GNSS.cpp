//#ifdef UNIT_TEST

#include <Arduino.h>
#include <HardwareSerial.h>
#include <SparkFun_u-blox_GNSS_Arduino_Library.h>
#include <unity.h>

#define TESTS_NUM 100
#define TESTS_DELAY 100

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_TEST(...)                  \
	INFO_ESP_PORT.print("INFO TEST: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

HardwareSerial gnssSerial(2);
SFE_UBLOX_GNSS gnss;

void test_location()
{
	long latitude = gnss.getLatitude();
	INFO_TEST("Latitude: %lu\n", latitude);

	long longitude = gnss.getLongitude();
	INFO_TEST("Longitude: %lu\n", longitude);

	long altitude = gnss.getAltitude();
	INFO_TEST("Altitude: %lu\n", altitude);

	TEST_ASSERT_TRUE(latitude != 0);
	TEST_ASSERT_TRUE(longitude != 0);
	TEST_ASSERT_TRUE(altitude != 0);
}

void test_time()
{
	String time;
	uint32_t year;
	uint8_t hour, minute, second, milisecond, month, day;

	year = gnss.getYear();
	month = gnss.getMonth();
	day = gnss.getDay();
	hour = gnss.getHour();
	minute = gnss.getMinute();
	second = gnss.getSecond();
	milisecond = gnss.getMillisecond();

	time.concat(year);
	time.concat("-");
	time.concat(month);
	time.concat("-");
	time.concat(day);
	time.concat(" ");

	time.concat(hour);
	time.concat(":");
	time.concat(minute);
	time.concat(":");
	time.concat(second);
	time.concat(".");
	time.concat(milisecond);


	INFO_TEST("TIME %s\n", time.c_str());

	TEST_ASSERT_TRUE(year >= 2021);
	TEST_ASSERT_TRUE(month >= 1 && month <= 12);
	TEST_ASSERT_TRUE(day >= 1 && month <= 31);
	TEST_ASSERT_TRUE(hour >= 1 && month <= 23);
	TEST_ASSERT_TRUE(minute >= 0 && minute <= 59);
	TEST_ASSERT_TRUE(second >= 0 && second <= 59);
}

void setup()
{
	// NOTE!!! Wait for >2 secs
	// if board doesn't support software reset via Serial.DTR/RTS
	delay(2000);
	Serial.begin(115200);
	gnssSerial.setTimeout(2000);
	INFO_TEST("Starting setup\n");
	for (uint8_t u = 0; u < 3; u++)
	{
		INFO_TEST("Trying 38400 baud\n");
		gnssSerial.begin(38400);
		if (gnss.begin(gnssSerial) == true)
		{
			INFO_TEST("GNSS connected at 38400 baud\n");
			break;
		}

		INFO_TEST("Trying 9600 baud\n");
		gnssSerial.begin(9600);
		//gnssSerial.setTimeout(2000);
		if (gnss.begin(gnssSerial) == true)
		{
			INFO_TEST("GNSS connected at 9600 baud\n");
			break;
		} else {
			INFO_TEST("GNSS not detected\n");
			delay(2000);
		}
	}

	gnss.setUART1Output(COM_TYPE_UBX);
	//gnss.setUART1Output(COM_TYPE_NMEA);
	//gnss.setNMEAOutputPort(gnssSerial);
	//gnss.setUART2Output(COM_TYPE_UBX);
	gnss.setI2COutput(COM_TYPE_UBX);
	//gnss.saveConfiguration();
	gnss.enableDebugging();
	UNITY_BEGIN(); // IMPORTANT LINE!
}

uint8_t u = 0;

void loop()
{
	if (u < TESTS_NUM)
	{
		// INFO_TEST("Se chequea recibir una sentencia NMEA\n");
		// RUN_TEST(test_NMEA);
		//INFO_TEST("Se chequea recibir sentencias NMEA\n");
		//RUN_TEST(test_nmea);

		// INFO_TEST("Se chequea recibir la hora\n");
		//gnss.checkUblox();

		RUN_TEST(test_time);

		RUN_TEST(test_location);
		

		//RUN_TEST(test_mix);

		//delay(TESTS_DELAY);
		u++;
	}
	else
	{
		UNITY_END(); // stop unit testing
	}
	delay(1000);
}

//#endif
