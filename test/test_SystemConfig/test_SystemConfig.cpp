#ifdef UNIT_TEST

#include <SystemConfig.h>
#include <unity.h>

#ifndef TESTS_NUM
#define TESTS_NUM 2
#endif
#ifndef TESTS_DELAY
#define TESTS_DELAY 50
#endif

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_TEST(...)                  \
	INFO_ESP_PORT.print("INFO TEST: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

void test_begin()
{
	TEST_ASSERT_TRUE(config.begin());
}

void test_reset_config()
{
	TEST_ASSERT_TRUE(config.resetConfig());
}

void check_value(uint8_t key_u, const char *key, const char *value)
{
	String tmpStr;
	const char *ptmp;
	ptmp = config.getValue(key_u);
	TEST_ASSERT_EQUAL_STRING(value, ptmp);
	ptmp = config.getValue(key);
	TEST_ASSERT_EQUAL_STRING(value, ptmp);
	ptmp = config.getValue(String(key));
	TEST_ASSERT_EQUAL_STRING(value, ptmp);

	tmpStr = config.getValueStr(key_u);
	TEST_ASSERT_EQUAL_STRING(value, tmpStr.c_str());
	tmpStr = config.getValueStr(String(key));
	TEST_ASSERT_EQUAL_STRING(value, tmpStr.c_str());
	tmpStr = config.getValueStr(key);
	TEST_ASSERT_EQUAL_STRING(value, tmpStr.c_str());
}

void test_get_default_value()
{
	uint8_t TEST_KEY_U = SC_WIFI_SSID;
	const char *TEST_KEY = "wifi_ssid";
	const char *TEST_VALUE = "smmr";

	TEST_ASSERT_TRUE(config.resetConfig());

	check_value(TEST_KEY_U, TEST_KEY, TEST_VALUE);
}

void test_set_value()
{
	uint8_t TEST_KEY_U = SC_WIFI_SSID;
	const char *TEST_KEY = "wifi_ssid";
	const char *TEST_VALUE1 = "TEST_SMMR1";
	const char *TEST_VALUE2 = "TEST_SMMR2";
	String TEST_VALUE1_STR = TEST_VALUE1;
	String TEST_VALUE2_STR = TEST_VALUE2;

	TEST_ASSERT_TRUE(config.resetConfig());

	config.setValue(TEST_KEY_U, TEST_VALUE1);
	check_value(TEST_KEY_U, TEST_KEY, TEST_VALUE1);

	config.setValue(TEST_KEY_U, TEST_VALUE2_STR);
	check_value(TEST_KEY_U, TEST_KEY, TEST_VALUE2);

	config.setValue(TEST_KEY, TEST_VALUE1);
	check_value(TEST_KEY_U, TEST_KEY, TEST_VALUE1);

	config.setValue(TEST_KEY, TEST_VALUE2_STR);
	check_value(TEST_KEY_U, TEST_KEY, TEST_VALUE2);
}

void test_set_invalid_value()
{
	uint8_t TEST_KEY_U = SC_WIFI_CHANNEL;
	const char *TEST_KEY = "wifi_channel";
	const char *TEST_VALUE1 = "A";
	const char *TEST_VALUE2 = "pp";

	String TEST_VALUE1_STR = TEST_VALUE1;
	String TEST_VALUE2_STR = TEST_VALUE2;

	TEST_ASSERT_TRUE(config.resetConfig());

	TEST_ASSERT_FALSE(config.setValue(TEST_KEY_U, TEST_VALUE1));

	TEST_ASSERT_FALSE(config.setValue(TEST_KEY_U, TEST_VALUE2_STR));

	TEST_ASSERT_FALSE(config.setValue(TEST_KEY, TEST_VALUE1));

	TEST_ASSERT_FALSE(config.setValue(TEST_KEY, TEST_VALUE2_STR));
}

void setup()
{
	// NOTE!!! Wait for >2 secs
	// if board doesn't support software reset via Serial.DTR/RTS
	delay(2000);

	UNITY_BEGIN(); // IMPORTANT LINE!

	// config.init();
}

uint8_t i = 0;

void loop()
{
	if (i < TESTS_NUM)
	{
		RUN_TEST(test_begin);

		RUN_TEST(test_reset_config);

		RUN_TEST(test_get_default_value);

		RUN_TEST(test_set_value);

		RUN_TEST(test_set_invalid_value);

		config.end();

		delay(TESTS_DELAY);
		i++;
	}
	else
	{
		UNITY_END(); // stop unit testing
	}
}

#endif
