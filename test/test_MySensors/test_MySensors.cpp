
#include <Arduino.h>
#include <MySensors.h>
#include <unity.h>

#ifndef TESTS_NUM
#define TESTS_NUM 2
#endif
#ifndef TESTS_DELAY
#define TESTS_DELAY 50
#endif

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_TEST(...)                  \
	INFO_ESP_PORT.print("INFO TEST: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

void test_init()
{
	TEST_ASSERT(sensors.init());
	/* wait for sensor to acquire a few seconds */
	delay(2000);
}

void test_first_update()
{
	uint8_t u;
	bool ret;
	/* ignore up to 15 first updates */
	for (u=0; u < 15; u++) {
		ret = sensors.update();
		if (ret)
			break;
	}
	TEST_ASSERT(ret);
}

void test_first_getCO2()
{
	uint16_t minCo2 = 100;
	uint16_t maxCo2 = 2000;
	uint16_t co2;
	uint32_t i;
	bool ret = false;
	for (i = 1; i < 10;  i++) {
		TEST_ASSERT(sensors.update());
		co2 = sensors.getCO2();
		//INFO_TEST("sensors.getCO2() returns %d\n", co2);
		if (co2 > minCo2 && co2 < maxCo2) {
			ret = true;
			break;
		}
	}
	TEST_ASSERT(ret);
}

void test_getValues()
{
	uint16_t minCo2 = 100;
	uint16_t maxCo2 = 2000;
	float minTemperature = 0;
	float maxTemperature = 50;
	uint16_t co2;
	float temperature;
	uint32_t i;
	bool ret;
	for (i = 1; i < 10;  i++) {
		TEST_ASSERT(sensors.update());
		co2 = sensors.getCO2();
		temperature = sensors.getTemp();
		INFO_TEST("co2:%d temp:%f\n", co2, temperature);
		TEST_ASSERT(co2 > minCo2 && co2 < maxCo2);
		TEST_ASSERT(temperature > minTemperature && temperature < maxTemperature);
	}
}

void setup()
{
	// NOTE!!! Wait for >2 secs
	// if board doesn't support software reset via Serial.DTR/RTS
	delay(2000);

	Serial.begin(115200);
	UNITY_BEGIN(); // IMPORTANT LINE!
}

uint8_t i = 0;

void loop()
{
	if (i < TESTS_NUM)
	{
		RUN_TEST(test_init);

		RUN_TEST(test_first_update);

		RUN_TEST(test_first_getCO2);

		RUN_TEST(test_getValues);

		delay(TESTS_DELAY);
		i++;
	}
	else
	{
		UNITY_END(); // stop unit testing
	}
}
