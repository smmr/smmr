//#ifdef UNIT_TEST

#include <Arduino.h>
#include <HardwareSerial.h>
#include <TinyGPS++.h>
#include <unity.h>

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_TEST(...)                  \
	INFO_ESP_PORT.print("INFO TEST: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

HardwareSerial gpsSerial(2);
TinyGPSPlus gps;

void test_begin()
{
	//TEST_ASSERT_TRUE(gps.begin());
}

void test_nmea()
{
	String line = gpsSerial.readStringUntil('\n');
	INFO_TEST("%s\n", line.c_str());

	// minimal line size
	TEST_ASSERT_TRUE(line.length() > 9);

	// check start byte
	TEST_ASSERT_EQUAL_INT8('$', line.c_str()[0]);

	// check CRC * separator
	TEST_ASSERT_EQUAL_INT8('*', line.c_str()[line.length() - 4]);
}

bool refreshGps()
{
	while (1)
	{
		while (gpsSerial.available())
			if (gps.encode(gpsSerial.read()))
				return true;
	}
}

void test_time()
{
	String time;
	uint32_t year;
	uint8_t hour, minute, second, month, day;

	refreshGps();
	year = gps.date.year();
	month = gps.date.month();
	day = gps.date.day();
	hour = gps.time.hour();
	minute = gps.time.minute();
	second = gps.time.second();

	time.concat(year);
	time.concat("-");
	time.concat(month);
	time.concat("-");
	time.concat(day);
	time.concat(" ");

	time.concat(hour);
	time.concat(":");
	time.concat(minute);
	time.concat(":");
	time.concat(second);

	INFO_TEST("TIME %s\n", time.c_str());

	TEST_ASSERT_TRUE(year >= 2020);
	TEST_ASSERT_TRUE(month >= 1 && month <= 12);
	TEST_ASSERT_TRUE(day >= 1 && month <= 31);
	//TEST_ASSERT_TRUE(hour >= 1 && month <= 31);
	TEST_ASSERT_TRUE(minute >= 0 && minute <= 59);
	TEST_ASSERT_TRUE(second >= 0 && second <= 59);
}

void test_mix()
{
	String line, time;
	uint32_t u, year;
	uint8_t hour, minute, second, month, day;

	line = gpsSerial.readStringUntil('\n');
	INFO_TEST("%s\n", line.c_str());

	// minimal line size
	TEST_ASSERT_TRUE(line.length() > 9);

	// check start byte
	TEST_ASSERT_EQUAL_INT8('$', line.c_str()[0]);

	// check CRC * separator
	TEST_ASSERT_EQUAL_INT8('*', line.c_str()[line.length() - 4]);

	for (u = 0; u < line.length(); u++)
	{
		if (gps.encode(line.c_str()[u]))
		{

			year = gps.date.year();
			month = gps.date.month();
			day = gps.date.day();
			hour = gps.time.hour();
			minute = gps.time.minute();
			second = gps.time.second();

			time.concat(year);
			time.concat("-");
			time.concat(month);
			time.concat("-");
			time.concat(day);
			time.concat(" ");

			time.concat(hour);
			time.concat(":");
			time.concat(minute);
			time.concat(":");
			time.concat(second);

			INFO_TEST("TIME %s\n", time.c_str());

			TEST_ASSERT_TRUE(year >= 2020);
			TEST_ASSERT_TRUE(month >= 1 && month <= 12);
			TEST_ASSERT_TRUE(day >= 1 && month <= 31);
			//TEST_ASSERT_TRUE(hour >= 1 && month <= 31);
			TEST_ASSERT_TRUE(minute >= 0 && minute <= 59);
			TEST_ASSERT_TRUE(second >= 0 && second <= 59);
		}
	}
}

void setup()
{
	// NOTE!!! Wait for >2 secs
	// if board doesn't support software reset via Serial.DTR/RTS
	delay(2000);
	gpsSerial.begin(9600);
	gpsSerial.setTimeout(2000);

	UNITY_BEGIN(); // IMPORTANT LINE!

	INFO_TEST("Se inicializa gps\n");
	RUN_TEST(test_begin);
}

uint8_t i = 0;
uint8_t count = 5;

void loop()
{
	//if (i < count) {
	if (1)
	{
		// INFO_TEST("Se chequea recibir una sentencia NMEA\n");
		// RUN_TEST(test_NMEA);
		//INFO_TEST("Se chequea recibir sentencias NMEA\n");
		//RUN_TEST(test_nmea);

		// INFO_TEST("Se chequea recibir la hora\n");
		// RUN_TEST(test_time);

		INFO_TEST("Se chequean ambos nmea/hora y se imprimen para contrastar\n");
		RUN_TEST(test_mix);

		//delay(500);
		i++;
	}
	else if (i == count)
	{
		UNITY_END(); // stop unit testing
	}
}

//#endif
