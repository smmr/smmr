#ifdef UNIT_TEST

#include <Arduino.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <MQTT.h>
#include <unity.h>

#define TESTS_NUM 10
#define TESTS_DELAY 100

#define WIFI_SSID "SMMRNETOLD"
#define WIFI_PASSWORD "smmrnet00"

#define MQTT_DEVICE "dut"
//#define MQTT_HOST "192.168.11.1"
//#define MQTT_HOST "192.168.12.2"
//#define MQTT_HOST "broker.emqx.io"
#define MQTT_HOST "broker.sguarin.com.ar"
#define MQTT_PORT 8883
#define MQTT_USER "test"
#define MQTT_PASSWORD "test00"
#define MQTT_TOPIC "smmrtest"

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_TEST(...)                  \
	INFO_ESP_PORT.print("INFO TEST: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

const char *server_fingerprint = "B8:9C:7D:12:B3:A0:19:D3:DF:9A:34:89:0B:62:2A:C3:DF:D5:83:43:F5:07:B3:9E:10:0F:C5:DC:D5:DD:2D:AC";
const char* not_server_fingerprint = "41:C3:FE:9E:C5:C6:36:04:02:8F:FC:5A:BF:4E:3F:98:A9:73:F5:F5:6A:D4:3B:23:4A:1D:6D:82:1E:06:31:E4";

MQTTClient mqttclient;
WiFiClientSecure netclient;

void test_wifi_connect()
{
	bool ret;
	char counter = 0;

	INFO_TEST("Connecting to WIFI...\n");
	WiFi.disconnect();
	//WiFi.config(IPAddress(192, 168, 5, 2), IPAddress(192, 168, 5, 1), IPAddress(255, 255, 255, 0), IPAddress(8, 8, 8, 8));
	WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
	ret = true;
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		INFO_TEST("Trying reconnect to WIFI\n");
		if (counter == 20)
		{
			ret = false;
			break;
		}
		counter++;
	}
	TEST_ASSERT_TRUE(ret);
}

void test_mqtt_connect_tls_finger()
{
	bool ret;
	mqttclient.begin(MQTT_HOST, MQTT_PORT, netclient);
	ret = mqttclient.connect(MQTT_DEVICE, MQTT_USER, MQTT_PASSWORD);
	if (netclient.verify(server_fingerprint, MQTT_HOST)) {
		INFO_TEST("Certification validated by fingerprint");
	} else {
		TEST_ASSERT_TRUE(false);
	}
	TEST_ASSERT_TRUE(ret);
}

void test_mqtt_connect_tls_finger_fail()
{
	bool ret;
	mqttclient.begin(MQTT_HOST, MQTT_PORT, netclient);
	ret = mqttclient.connect(MQTT_DEVICE, MQTT_USER, MQTT_PASSWORD);
	if (netclient.verify(not_server_fingerprint, MQTT_HOST)) {
		TEST_ASSERT_TRUE(false);
	} else {
		INFO_TEST("Certification validated by fingerprint");
		TEST_ASSERT_TRUE(true);
	}
}

void test_mqtt_publish()
{
	bool ret;
	ret = mqttclient.publish(MQTT_TOPIC, "hola que tal");
	ret &= mqttclient.publish(MQTT_TOPIC, "hola que tal 2");
	ret &= mqttclient.publish(MQTT_TOPIC, "hola que tal 3");
	ret &= mqttclient.publish(MQTT_TOPIC, "hola que tal 4");
	ret &= mqttclient.publish(MQTT_TOPIC, "hola que tal 5");
}

void test_mqtt_disconnect()
{
	bool ret;
	mqttclient.disconnect();
	ret = true;
	TEST_ASSERT_TRUE(ret);
}

void setup()
{
	// NOTE!!! Wait for >2 secs
	// if board doesn't support software reset via Serial.DTR/RTS
	delay(2000);
	
	UNITY_BEGIN(); // IMPORTANT LINE!

	RUN_TEST(test_wifi_connect);
}

uint8_t i = 0;

void loop()
{


	if (i < TESTS_NUM)
	{
		RUN_TEST(test_mqtt_connect_tls_finger_fail);
		RUN_TEST(test_mqtt_connect_tls_finger);
		RUN_TEST(test_mqtt_publish);
		RUN_TEST(test_mqtt_disconnect);

		delay(TESTS_DELAY);
		i++;
	}

	if (i == TESTS_NUM)
	{
		UNITY_END(); // stop unit testing
	}
}

#endif
