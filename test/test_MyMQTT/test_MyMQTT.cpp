#include <Arduino.h>
#include <WiFi.h>
#include <MyMQTT.h>
#include <unity.h>

#define TESTS_NUM 1
#define TESTS_DELAY 100

#ifndef WIFI_SSID
#define WIFI_SSID "SMMRNET"
#endif
#ifndef WIFI_PASSWORD
#define WIFI_PASSWORD "smmrnet"
#endif

#define MQTT_DEVICE "test"
#define MQTT_HOST "smmr.sguarin.com.ar"
#define MQTT_TOPIC "smmrdev"
#ifndef MQTT_USERNAME
#define MQTT_USERNAME "test"
#endif
#ifndef MQTT_PASSWORD
#define MQTT_PASSWORD "test"
#endif
#define MQTT_IVALID_USER "testbad"
#define MQTT_INVALID_PASSWORD "test00bad"

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_TEST(...)                  \
	INFO_ESP_PORT.print("INFO TEST: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

const char* root_ca= 
        "-----BEGIN CERTIFICATE-----\n" \
        "MIIDozCCAougAwIBAgIUOHuHU3KrbRMZiWQSifsv+EzL5RYwDQYJKoZIhvcNAQEL\n" \
        "BQAwYTELMAkGA1UEBhMCQVIxDTALBgNVBAgMBENBQkExDTALBgNVBAcMBENBQkEx\n" \
        "DDAKBgNVBAoMA01TRTEMMAoGA1UECwwDU0VEMRgwFgYDVQQDDA9NU0UtU0VEIEFD\n" \
        "IFJhaXowHhcNMjEwNTA5MjE1MTQ0WhcNMjEwNjA4MjE1MTQ0WjBhMQswCQYDVQQG\n" \
        "EwJBUjENMAsGA1UECAwEQ0FCQTENMAsGA1UEBwwEQ0FCQTEMMAoGA1UECgwDTVNF\n" \
        "MQwwCgYDVQQLDANTRUQxGDAWBgNVBAMMD01TRS1TRUQgQUMgUmFpejCCASIwDQYJ\n" \
        "KoZIhvcNAQEBBQADggEPADCCAQoCggEBAKNs2WX/dscsBriXaE+qOQBEjk0GXL3I\n" \
        "f+6H41rx/1dG1yHXTsyvQLo/FCdzI+EeijZapIwQK3KVyHSWlzausz+xgGuggc+j\n" \
        "ZXJIhq0rBHgkm/jT2hGJgSbU3Y1jWAZRjpo6cWrQJeyez19uTMJcNovR1yDog/UF\n" \
        "7y9f+tZGCMXszRps9KrIczhZD3SH0FfOl20gqNFI6oSd/Gy7CeIYX9RNrly1RIDo\n" \
        "zWklynXVjQRiLmgDMBmRMF+UDBv0SF71RdH4a55NBrOAYCC7APsUsuXS64YWJazm\n" \
        "W2g5FhS5IXio85zlNj7f22+MslLQcVKQzPPjEIAXulIyn11zIKDs/FUCAwEAAaNT\n" \
        "MFEwHQYDVR0OBBYEFIYCzAllB6c6rnBIrotPbPNePCbsMB8GA1UdIwQYMBaAFIYC\n" \
        "zAllB6c6rnBIrotPbPNePCbsMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQEL\n" \
        "BQADggEBAHdSFaAX2O+0EXmRkQsmUNxSd1mmYr4rTgVrOChomkHtZg/ngLtTsoHW\n" \
        "gJ6RjHlQ4GoKkceN1L9TTXHiaVS6yiSW/3U/JHg3rDNm/6BARiDbhLZltgEQL71j\n" \
        "Z5juivO5D+6x8lchv6y+FEyHJsi/hGCFSaf2ulL/eFyrAGYtcyklKIMbYkS4n4il\n" \
        "7IArGkL/rRpzgTrB9AZxKNkeCLJyoEjfGUtFOj4R0cgDGzIfGr1OVcCuqsv/ILgL\n" \
        "D8/aToOOC3AkhyTH+UreXfzvwrAGp1h5QluRe428HQCEU+3rc2Mtjx4oyWwOqLal\n" \
        "ivV1ajmXYnitpwtvynoikERw20RE+6w=\n" \
        "-----END CERTIFICATE-----\n";

void test_wifi_connect()
{
	bool ret;
	char counter = 0;

	INFO_TEST("Connecting to WIFI SSID:%s PASSWORD:%s...\n", WIFI_SSID, WIFI_PASSWORD);
	WiFi.disconnect();
	//WiFi.config(IPAddress(192, 168, 5, 2), IPAddress(192, 168, 5, 1), IPAddress(255, 255, 255, 0), IPAddress(8, 8, 8, 8));
	WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
	ret = true;
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		INFO_TEST("Trying reconnect to WIFI\n");
		if (counter == 20)
		{
			ret = false;
			break;
		}
		counter++;
	}
	TEST_ASSERT_TRUE(ret);
}

void test_mqtt_connect()
{
	bool ret;
	mqtt.begin(MQTT_DEVICE, MQTT_HOST, MQTT_USERNAME, MQTT_PASSWORD, MQTT_TOPIC, MyMQTTMode::MODE_MQTT, false);
	ret = mqtt.connect();
	TEST_ASSERT_TRUE(ret);
}

void test_mqtt_tls_connect()
{
	bool ret;
	mqtt.begin(MQTT_DEVICE, MQTT_HOST, MQTT_USERNAME, MQTT_PASSWORD, MQTT_TOPIC, MyMQTTMode::MODE_MQTTTLS, false);
	ret = mqtt.connect();
	TEST_ASSERT_TRUE(ret);
}

void test_mqtt_tls_ca_connect()
{
	bool ret;
	mqtt.setCAPath("/ca/ca.pem");
	mqtt.begin(MQTT_DEVICE, MQTT_HOST, MQTT_USERNAME, MQTT_PASSWORD, MQTT_TOPIC, MyMQTTMode::MODE_MQTTTLS_CA, false);
	ret = mqtt.connect();
	TEST_ASSERT_TRUE(ret);
}

void test_mqtt_tls_invalid_ca_connect()
{
	bool ret;
	mqtt.setCAPath("/ca/sofse.pem");
	mqtt.begin(MQTT_DEVICE, MQTT_HOST, MQTT_USERNAME, MQTT_PASSWORD, MQTT_TOPIC, MyMQTTMode::MODE_MQTTTLS_CA, false);
	//mqtt.setCACert(root_ca);
	ret = mqtt.connect();
	TEST_ASSERT_FALSE(ret);
}

void test_mqtt_publish()
{
	bool ret;
	String msg = String("Hello World");
	ret = mqtt.publish(msg);
	ret &= mqtt.publish(msg);
	TEST_ASSERT_TRUE(ret);
}

void test_mqtt_disconnect()
{
	bool ret;
	ret = mqtt.end();
	ret = true;
	TEST_ASSERT_TRUE(ret);
}

void test_mqtt_tls_connect_invalid_password()
{
	bool ret;
	mqtt.begin(MQTT_DEVICE, MQTT_HOST, MQTT_USERNAME, MQTT_INVALID_PASSWORD, MQTT_TOPIC, MyMQTTMode::MODE_MQTTTLS, false);
	ret = mqtt.connect(2);
	TEST_ASSERT_FALSE(ret);
}

void setup()
{
	// NOTE!!! Wait for >2 secs
	// if board doesn't support software reset via Serial.DTR/RTS
	delay(2000);
	
	UNITY_BEGIN();

	RUN_TEST(test_wifi_connect);
}

uint8_t i = 0;

void loop()
{
	if (i < TESTS_NUM)
	{
		// Commented because server does not support
		// non TLS connections
		RUN_TEST(test_mqtt_connect);
		RUN_TEST(test_mqtt_publish);
		RUN_TEST(test_mqtt_disconnect);

		// There is a bug, when setting tls without ca validation
		// upstream api don't permit to set with validation on same class
		RUN_TEST(test_mqtt_tls_invalid_ca_connect);
		RUN_TEST(test_mqtt_disconnect);

		RUN_TEST(test_mqtt_tls_ca_connect);
		RUN_TEST(test_mqtt_publish);
		RUN_TEST(test_mqtt_disconnect);

		RUN_TEST(test_mqtt_tls_connect_invalid_password);
		test_mqtt_disconnect();
		
		RUN_TEST(test_mqtt_tls_connect);
		RUN_TEST(test_mqtt_publish);
		RUN_TEST(test_mqtt_disconnect);

		delay(TESTS_DELAY);
		i++;
	}
	else
	{
		UNITY_END();
	}
}
