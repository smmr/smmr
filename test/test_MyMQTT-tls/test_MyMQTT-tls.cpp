#ifdef UNIT_TEST

#include <Arduino.h>
#include <WiFi.h>
#include <MyMQTT.h>
#include <unity.h>

#define TESTS_NUM 10
#define TESTS_DELAY 100

#define WIFI_SSID "SMMRNETOLD"
#define WIFI_PASSWORD "smmrnet00"

#define MQTT_DEVICE "dut"
//#define MQTT_HOST "192.168.11.1"
//#define MQTT_HOST "192.168.12.2"
//#define MQTT_HOST "broker.emqx.io"
#define MQTT_HOST "broker.sguarin.com.ar"
#define MQTT_TOPIC "smmrtest"
#define MQTT_USER "test"
#define MQTT_PASSWORD "test00"

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_TEST(...)                  \
	INFO_ESP_PORT.print("INFO TEST: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

const char* root_ca= \
        "-----BEGIN CERTIFICATE-----" \
        "MIIDozCCAougAwIBAgIUD5jKSc9puKfmTYMbZ392NcGh8kEwDQYJKoZIhvcNAQEL" \
        "BQAwYTELMAkGA1UEBhMCQVIxDTALBgNVBAgMBENBQkExDTALBgNVBAcMBENBQkEx" \
        "DDAKBgNVBAoMA01TRTEMMAoGA1UECwwDU0VEMRgwFgYDVQQDDA9NU0UtU0VEIEFD" \
        "IFJhaXowHhcNMjAxMjA2MDMxMTA4WhcNMjEwMTA1MDMxMTA4WjBhMQswCQYDVQQG" \
        "EwJBUjENMAsGA1UECAwEQ0FCQTENMAsGA1UEBwwEQ0FCQTEMMAoGA1UECgwDTVNF" \
        "MQwwCgYDVQQLDANTRUQxGDAWBgNVBAMMD01TRS1TRUQgQUMgUmFpejCCASIwDQYJ" \
        "KoZIhvcNAQEBBQADggEPADCCAQoCggEBAPM9evNTgeH5tK96NhdlkZiD5Jf9RbIV" \
        "RTotXb53fcuhawFtGeXSb9dGv5LoSpKUhdA55lG2Cf7jP7o9Hw/QH85XQwjdmLs+" \
        "Rqj0DdPuiLsHB0PHfgrjDPmjQpwlUU3HhEdYRwuDvF8Wgzk+CfPMq4C65iwM8TVB" \
        "8nYjkChXJ0DZJrMgNTLxGhizsGPz9Qklu21lvvyh2q0m1L89qrNL0lY27KDsyBRN" \
        "TVsDisLKOmPBOhFvR8eof0TggGItcmt8sqgL8hNLbCHXqCTOH4t7++jbBh6/njLq" \
        "EjiztioI26NaKR0lzCNgLtTxEDAZnvXjqIU2UpxmLhD/PbtLOefXwL0CAwEAAaNT" \
        "MFEwHQYDVR0OBBYEFPatL8fDvuFgoG8Zby37RFuQSUORMB8GA1UdIwQYMBaAFPat" \
        "L8fDvuFgoG8Zby37RFuQSUORMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQEL" \
        "BQADggEBAMjbT75O0/PHXHB8sqndAbzHV612WZV1ZuXjg1FnYBiv+LaNRYcinGFC" \
        "lWfDYM8DmorPcmSMfSQtf7l8fq2pyxhfstC6Du9Az6MnUk/Z8zZY09uTbbSZ+3V4" \
        "O0rPxihf5uG1UxlzJ8Y3EKxwf6+Ot3EWnx1pfNIXDDo1NKf/OG/X6YzKet9itoiL" \
        "sFS2d56Rsrz/X/yzlBv48djTs+jKRdRipKFpLq+NpjlPGk6KWUV58/uAdowfOGqD" \
        "Iwv2sF3zBVGV+pdjI3Y40UCCjnAp6q6i5Xs30eAq7QhkEC8Hztho/jwz7D2tOXOK" \
        "2QADjHEsGmcxKwG2wENSFsAjbPtrwuM=" \
        "-----END CERTIFICATE-----";

void test_wifi_connect()
{
	bool ret;
	char counter = 0;

	INFO_TEST("Connecting to WIFI...\n");
	WiFi.disconnect();
	//WiFi.config(IPAddress(192, 168, 5, 2), IPAddress(192, 168, 5, 1), IPAddress(255, 255, 255, 0), IPAddress(8, 8, 8, 8));
	WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
	ret = true;
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		INFO_TEST("Trying reconnect to WIFI\n");
		if (counter == 20)
		{
			ret = false;
			break;
		}
		counter++;
	}
	TEST_ASSERT_TRUE(ret);
}

void test_mqtt_connect_tls()
{
	bool ret;
	mqtt.begin(MQTT_DEVICE, MQTT_HOST, MQTT_USER, MQTT_PASSWORD, true);
	ret = mqtt.connect();
	TEST_ASSERT_TRUE(ret);
}

void test_mqtt_publish()
{
	bool ret;
	ret = mqtt.publish(MQTT_TOPIC, "hola que tal");
	ret &= mqtt.publish(MQTT_TOPIC, "hola que tal 2");
	ret &= mqtt.publish(MQTT_TOPIC, "hola que tal 3");
	ret &= mqtt.publish(MQTT_TOPIC, "hola que tal 4");
	ret &= mqtt.publish(MQTT_TOPIC, "hola que tal 5");
}

void test_mqtt_disconnect()
{
	bool ret;
	mqtt.end();
	ret = true;
	TEST_ASSERT_TRUE(ret);
}

void setup()
{
	// NOTE!!! Wait for >2 secs
	// if board doesn't support software reset via Serial.DTR/RTS
	delay(2000);
	
	UNITY_BEGIN(); // IMPORTANT LINE!

	RUN_TEST(test_wifi_connect);
}

uint8_t i = 0;

void loop()
{


	if (i < TESTS_NUM)
	{

		RUN_TEST(test_mqtt_connect_tls);
		RUN_TEST(test_mqtt_publish);
		RUN_TEST(test_mqtt_disconnect);

		delay(TESTS_DELAY);
		i++;
	}

	if (i == TESTS_NUM)
	{
		UNITY_END(); // stop unit testing
	}
}

#endif
