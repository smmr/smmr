#ifdef UNIT_TEST

#include <Arduino.h>
#include <MyInputs.h>
#include <unity.h>

#define TESTS_NUM 10
#define TESTS_DELAY 500

#ifdef DEBUG_ESP_PORT
#define INFO_ESP_PORT DEBUG_ESP_PORT
#else
#define INFO_ESP_PORT Serial
#endif
#define INFO_TEST(...)                  \
	INFO_ESP_PORT.print("INFO TEST: "); \
	INFO_ESP_PORT.printf(__VA_ARGS__)

bool test_begin(uint32_t rate)
{
	bool ret;
	ret = inputs.begin(rate);
	return ret;
}

void test_begin_10()
{
	bool ret;
	ret = test_begin(10);
	TEST_ASSERT_TRUE(ret);
}

void test_begin_100()
{
	bool ret;
	ret = test_begin(100);
	TEST_ASSERT_TRUE(ret);
}

bool test_acquire(uint32_t rate)
{
	bool ret = true;
	uint32_t i;

	String a1 = "";
	String a2 = "";
	String d1 = "";
	String d2 = "";
	String d3 = "";
	String d4 = "";

	myInputsDataItem_t myInputsDataItem;

	ret &= inputs.read(&myInputsDataItem);
	a1.concat(myInputsDataItem.a[0]);
	a2.concat(myInputsDataItem.a[1]);
	d1.concat(myInputsDataItem.d[0] != 0);
	d2.concat(myInputsDataItem.d[1] != 0);
	d3.concat(myInputsDataItem.d[2] != 0);
	d4.concat(myInputsDataItem.d[3] != 0);

	for (i = 1; i < rate; i++)
	{
		ret &= inputs.read(&myInputsDataItem);
		a1.concat(",");
		a1.concat(myInputsDataItem.a[0]);
		a2.concat(",");
		a2.concat(myInputsDataItem.a[1]);
		d1.concat(myInputsDataItem.d[0] != 0);
		d2.concat(myInputsDataItem.d[1] != 0);
		d3.concat(myInputsDataItem.d[2] != 0);
		d4.concat(myInputsDataItem.d[3] != 0);
	}
	INFO_TEST("A1 Acquired: %s\n", a1.c_str());
	INFO_TEST("A2 Acquired: %s\n", a2.c_str());
	INFO_TEST("D1 Acquired: %s\n", d1.c_str());
	INFO_TEST("D2 Acquired: %s\n", d2.c_str());
	INFO_TEST("D3 Acquired: %s\n", d3.c_str());
	INFO_TEST("D4 Acquired: %s\n", d4.c_str());

	return ret;
}

void test_acquire_10()
{
	bool ret;
	ret = test_acquire(10);
	TEST_ASSERT_TRUE(ret);
}

void test_acquire_100()
{
	bool ret;
	ret = test_acquire(100);
	TEST_ASSERT_TRUE(ret);
}

void setup()
{
	// NOTE!!! Wait for >2 secs
	// if board doesn't support software reset via Serial.DTR/RTS
	delay(2000);

	UNITY_BEGIN(); // IMPORTANT LINE!
}

uint8_t i = 0;
uint8_t count = 10;

void loop()
{
	if (i < TESTS_NUM)
	{

		INFO_TEST("test_init\n");
		RUN_TEST(test_begin_10);

		INFO_TEST("test_acquire\n");
		RUN_TEST(test_acquire_10);

		delay(TESTS_DELAY);
		i++;
	}
	else
	{
		UNITY_END(); // stop unit testing
	}
}

#endif
